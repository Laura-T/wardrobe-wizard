//
//  Classes.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 28/03/23.
//

import Foundation
import LocalAuthentication
import os
import SwiftData
import CoreNFC
import SwiftUI

struct MatchInfo: Codable, Hashable {
 enum FormalityLevel: Codable {
  case undefined
  case sports
  case casual
  case formal
  case smart
  case ceremonial
 }
 var formalityLevel: FormalityLevel = FormalityLevel.undefined
 var styleNotes: String = ""
 init(formalityLevel: FormalityLevel = .undefined, styleNotes: String = "") {
  self.formalityLevel = formalityLevel
  self.styleNotes = styleNotes
 }
}

@Model class Wardrobe: Identifiable, Equatable, Hashable {
 static let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: String(describing: Wardrobe.self))
 var id: UUID = UUID()
 var name: String = ""
 @Relationship(deleteRule: .cascade, inverse: \Clothing.wardrobes) var clothes: [Clothing]? = [Clothing]()
 var unClothes: [Clothing] {
  self.clothes ?? []
 }
 var styles: [String: MatchInfo] = [:]
 init(id: UUID = UUID(), name: String = "", clothes: [Clothing] = [], styles: [String: MatchInfo] = [:]) {
  self.id = id
  self.name = name
  self.clothes = clothes
  self.styles = styles
 }
 func add(_ clothing: Clothing, addingMatches: [UUID]) {
  self.clothes?.append(clothing)
  clothing.order = self.unClothes.count
  clothing.wardrobes?.append(self)
  for i in self.unClothes {
   if addingMatches.contains(i.id) {
    i.matches.append(clothing.id)
   }
  }
 }
 func addMultiple(from multipleString: String, type: String, washingInfo: WashingInfo, matches: [UUID], crossMatches: Bool = false, automaticTypes: Bool = false, types: inout [String], sort: Bool, context: ModelContext) {
  let added = multipleString.components(separatedBy: "\n")
  let addedIDs = added.map { _ in
   UUID()
  }
  for i in added {
   let currentIndex = added.firstIndex(of: i)!
   var currentType = type
   if automaticTypes {
    for t in types {
     if i.lowercased().hasPrefix(t.lowercased()) {
      currentType = t
     }
    }
    if !types.contains(String(i.prefix(while: { $0 != " " }))) {
     let newType = String(i.prefix(while: { $0 != " " }))
     currentType = newType
     types.append(newType)
     if sort {
      types.sort()
      types.move(fromOffsets: [types.firstIndex(of: String(localized: "Non impostato"))!], toOffset: 0)
     }
    }
   }
   let createdClothing = Clothing(id: addedIDs[currentIndex], name: i, type: currentType, washingInfo: washingInfo, matches: matches)
   if crossMatches {
    createdClothing.matches += addedIDs.filter { $0 != createdClothing.id }
   }
   self.clothes?.append(createdClothing)
   createdClothing.order = self.unClothes.count
   createdClothing.wardrobes?.append(self)
   for i in self.unClothes {
    if matches.contains(i.id) {
     i.matches.append(createdClothing.id)
    }
   }
   context.insert(createdClothing)
   try! context.save()
  }
 }
 func edit(_ clothing: Clothing, parameters: [String: Any]) {
  clothing.type = parameters["type", default: clothing.type] as! String
  clothing.name = parameters["name", default: clothing.name] as! String
  clothing.color = parameters["color", default: clothing.unColor] as! [ClothingColor]
  clothing.dominantColor = (parameters["color", default: clothing.unColor] as! [ClothingColor]).first
  clothing.colorOrder = parameters["colorOrder", default: clothing.colorOrder] as! [Int: String]
  clothing.patternName = parameters["pattern", default: clothing.patternName] as! String
  clothing.notes = parameters["notes", default: clothing.notes] as! String
  clothing.clothingDescription = parameters["description", default: clothing.clothingDescription] as! String
  clothing.washingInfo = parameters["washingInfo", default: clothing.washingInfo] as! WashingInfo
  clothing.image = parameters["image", default: clothing.image] as? Data
  clothing.matches = parameters["matches", default: clothing.matches] as! [UUID]
  let selectedMatches = clothing.matches
  for i in self.unClothes {
   if !selectedMatches.contains(i.id) && i.matches.contains(clothing.id) {
    print("running removing block")
    guard let ind = i.matches.firstIndex(of: clothing.id) else {
     print("Current clothing not found")
     return
    }
    print("Removing current clothing")
    i.matches.remove(at: ind)
    for key in self.styles.keys {
     let matchIds = key.components(separatedBy: " + ")
     if matchIds.contains(i.id.uuidString) {
      print("Removing related style info.")
      self.styles[key] = nil
     }
    }
   } else if selectedMatches.contains(i.id) && !i.matches.contains(clothing.id) {
    print("running adding block")
    i.matches.append(clothing.id)
   }
  }
 }
 func delete(_ clothing: Clothing, context: ModelContext) {
  print("deleting clothing")
  /*
  let clothingIndex = self.clothes?.firstIndex(of: clothing)
  print("clothing:", clothing.name, clothing.id)
  self.clothes?.remove(at: clothingIndex!)
  if let wardrobeIndex = clothing.wardrobes?.firstIndex(of: self) {
   clothing.wardrobes?.remove(at: wardrobeIndex)
  }
   */
  for i in self.unClothes {
   context.insert(i)
   if i.order > clothing.order {
    i.order = i.order-1
   }
   if i.matches.contains(clothing.id) {
    guard let ind = i.matches.firstIndex(of: clothing.id) else {
     return
    }
    i.matches.remove(at: ind)
   }
   for key in self.styles.keys {
    let matchIds = key.components(separatedBy: " + ")
    if matchIds.contains(clothing.id.uuidString) {
     print("Removing related style info.")
     self.styles[key] = nil
    }
   }
   try! context.save()
  }
  context.delete(clothing)
 }
 func reorder(source: IndexSet, destination: Int) {
  var currentOrder = self.clothes!.sorted(by: { $0.order < $1.order })
  currentOrder.move(fromOffsets: source, toOffset: destination)
  for (index, item) in currentOrder.enumerated() {
   item.order = index + 1
  }
 }
 // equatable conformance
 static func ==(lhs: Wardrobe, rhs: Wardrobe) -> Bool {
  return lhs.id == rhs.id
 }
 // hashable conformance
 func hash(into hasher: inout Hasher) {
  hasher.combine(id)
  hasher.combine(name)
  hasher.combine(clothes)
  hasher.combine(styles)
 }
}

struct WashingInfo: Codable, Hashable {
 enum WashingProgram: Codable, Hashable {
  case unspecified
  case darks
  case darkDelicates
  case lights
  case lightDelicates
  case darksOther
  case lightsOther
 }
 var washingProgram: WashingProgram = .unspecified {
  didSet {
   if washingProgram != .darksOther || washingProgram != .lightsOther {
    programName = ""
   }
  }
 }
 var washingNotes: String = ""
 var programName: String = ""
 init(washingProgram: WashingProgram = .unspecified, washingNotes: String = "", programName: String = "") {
  self.washingProgram = washingProgram
  self.washingNotes = washingNotes
  self.programName = programName
 }
 // hashable conformance
 func hash(into hasher: inout Hasher) {
  hasher.combine(washingProgram)
  hasher.combine(washingNotes)
  hasher.combine(programName)
 }
}

protocol Named {
 var name: String { get set }
}

@Model class Clothing: NSCopying, Identifiable, Equatable, Hashable, Named {
 var order: Int = 0
 var wardrobes: [Wardrobe]? = nil
 var unWardrobes: [Wardrobe] {
  return wardrobes ?? []
 }
 var id: UUID = UUID()
 var name: String = ""
 var type: String = ""
 var color: [ClothingColor]? = [ClothingColor]()
 var dominantColor: ClothingColor? = nil
 var colorOrder = [Int: String]()
 var unColor: [ClothingColor] {
  return colorOrder.sorted(by: { $0.key < $1.key }).compactMap { (position, name) in
   color?.first(where: { $0.name == name })
  }
 }
 var patternName: String = ""
 var notes: String = ""
 var clothingDescription: String = ""
 var washingInfo: WashingInfo = WashingInfo()
 var matches: [UUID] = []
 @Attribute(.externalStorage) var image: Data? = nil
 init(order: Int = 0, wardrobes: [Wardrobe]? = nil, id: UUID = UUID(), name: String = "", type: String = "", color: [ClothingColor]? = [ClothingColor](), dominantColor: ClothingColor? = nil, colorOrder: [Int: String] = [:], patternName: String = "", notes: String = "", description: String = "", washingInfo: WashingInfo = WashingInfo(), matches: [UUID] = [], image: Data? = nil) {
  self.order = order
  self.wardrobes = wardrobes
  self.id = id
  self.name = name
  self.type = type
  self.color = color
  self.dominantColor = dominantColor
  self.colorOrder = colorOrder
  self.patternName = patternName
  self.notes = notes
  self.clothingDescription = description
  self.washingInfo = washingInfo
  self.matches = matches
  self.image = image
 }
 // equatable conformance
 static func ==(lhs: Clothing, rhs: Clothing) -> Bool {
  return lhs.id == rhs.id
 }
 // NSCopying conformance
 func copy(with zone: NSZone? = nil) -> Any {
  let copy = Clothing(wardrobes: wardrobes, id: id, name: name, type: type, color: color, dominantColor: dominantColor, patternName: patternName, notes: notes, description: clothingDescription, washingInfo: washingInfo, matches: matches, image: image)
  return copy
 }
}

@Model class ClothingColor: Named, Hashable {
 enum Temperature: Int, Codable, Hashable {
  case cold, neutral, warm
 }
 var name: String = ""
 var temperature: Temperature? = nil
 var temperatureInt = 4
 var temperatureString: String {
  switch temperature {
  case nil: return String(localized: "Sconosciuto")
  case .cold: return String(localized: "Freddo")
  case .neutral: return String(localized: "Neutro")
  case .warm: return String(localized: "Caldo")
  }
 }
 @Relationship(deleteRule: .cascade, inverse: \Clothing.color) var coloredClothes: [Clothing]? = [Clothing]()
 var unColoredClothes: [Clothing] {
  return coloredClothes ?? []
 }
 @Relationship(deleteRule: .cascade, inverse: \Clothing.dominantColor) var dominants: [Clothing]? = nil
 init(name: String = "", temperature: Temperature? = nil, coloredClothes: [Clothing]? = []) {
  self.name = name
  self.temperature = temperature
  self.temperatureInt = temperature?.rawValue ?? 4
  self.coloredClothes = coloredClothes
 }
 func getMatchableColors(wardrobes: [Wardrobe]) -> [ClothingColor] {
  var result = [ClothingColor]()
  let allClothes = Array(wardrobes.map { $0.unClothes }.joined()).removingDuplicates()
  for clothing in allClothes {
   if (clothing.dominantColor ?? clothing.unColor.first) == self {
    let clothingMatches = clothing.matches.compactMap { id in allClothes.first(where: { $0.id == id }) }
    result += clothingMatches.compactMap { $0.dominantColor ?? $0.unColor.first }
   }
  }
  return result
 }
}

@Model class Settings {
 enum ClothesSortSetting: Codable {
  case name, colorName, colorTemperatureWarm, colorTemperatureCold, order
 }
 var currentClothesSorting: ClothesSortSetting = ClothesSortSetting.order
 var clothesSorting: [SortDescriptor<Clothing>] {
  switch currentClothesSorting {
  case .name: return [SortDescriptor(\Clothing.name, comparator: .localizedStandard)]
  case .colorName: return [SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)]
  case .colorTemperatureWarm: return [SortDescriptor(\Clothing.dominantColor?.temperatureInt, order: .reverse), SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)]
  case .colorTemperatureCold: return [SortDescriptor(\Clothing.dominantColor?.temperatureInt), SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)]
  case .order: return [SortDescriptor(\Clothing.order)]
  }
 }
 var clothesGrouping: ClothesGroupingCriteria? = nil
 var types: [String] = [String(localized: "Non impostato"), String(localized: "Camicia"), String(localized: "Felpa"), String(localized: "Gonna"), String(localized: "Maglietta"), String(localized: "Maglione"), String(localized: "Pantaloni"), String(localized: "Vestito intero")]
 var lastWardrobe: Int = 0
 var showWardrobeName: Bool = false
 var showClothesCount: Bool = false
 var showWardrobeIndicator: Bool = false
 @Attribute(.ephemeral) var unlocked = false
 var useBiometrics: Bool = false
 var highlightSelection: Bool = true
 var shakeForRandomMatch: Bool = true
 var temporaryDisableShake = false
 var hideNFCFeatures: Bool = false
 var speakWardrobeName: Bool = false
 var playSounds: Bool = true
 var automaticTypes: Bool = false
 var sortTypes: Bool = false
 init() {
 }
 func authenticate() {
  let context = LAContext()
  var error: NSError?
  // check whether biometric authentication is possible
  if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
   // it's possible, so go ahead and use it
   let reason = NSLocalizedString("L'accesso è richiesto per poter sbloccare l'app con i dati biometrici.", comment: "")
   context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
    // authentication has now completed
    if success {
     Task { @MainActor in
      self.unlocked = true
     }
    } else {
     // there was a problem
    }
   }
  } else {
   // no biometrics
  }
 }
}

extension NFCReader {
 func continuousRead() {
  self.firstInvalidates = false
  self.read()
 }
 func displayName(for message: String) -> String {
  var result: String
  let name = self.wardrobesToAnalyze?.compactMap { $0.unClothes.first(where: { $0.id.uuidString == message })?.name }.first
  result = name ?? "Sconosciuto"
  if self.speakWardrobeName {
   let wardrobes = self.wardrobesToAnalyze?.filter { $0.unClothes.contains(where: { $0.id.uuidString == message }) }.map { $0.name }.joined(separator: ", ")
   if let wardrobes {
    result += " (\(wardrobes))"
   }
  }
  if self.playSounds {
   if name != nil {
    Sounds.playSounds(soundfile: "351408__newagesoup__gleam-glow-sfx-chime.wav")
   } else {
    Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
   }
  }
  return result
 }
 func findClothing(for message: String) -> String {
  var result = ""
  if message == clothingToFind!.id.uuidString {
   if self.playSounds {
    Sounds.playSounds(soundfile: "119457__rmukx__revealed.wav")
   }
   result = String(localized: "Sì")
  } else {
   if self.playSounds {
    Sounds.playSounds(soundfile: "442873__euanmj__puffofsmoke.wav")
   }
   result = String(localized: "No")
  }
  return result
 }
 func detectClothing(for message: String) -> Clothing? {
  return self.wardrobesToAnalyze?.compactMap { $0.unClothes.first(where: { $0.id.uuidString == message }) }.first
 }
}

@Observable class NFCReader: NSObject, NFCNDEFReaderSessionDelegate {
 var startAlert = "Hold your iPhone near the tag."
 var endAlert = ""
 var msg = "Scan to read or Edit here to write..."
 var raw = "Raw Data available after scan."
 var session: NFCNDEFReaderSession?
 var firstInvalidates = true
 var wardrobesToAnalyze: [Wardrobe]? = nil
 var clothingToFind: Clothing? = nil
 var detectedClothing: Clothing? = nil
 var speakWardrobeName: Bool = false
 var playSounds: Bool = false
 var tagPositionTip = TagPositionTip()
 func read() {
  guard NFCNDEFReaderSession.readingAvailable else {
   print("Error")
   return
  }
  session = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: self.firstInvalidates)
  session?.alertMessage = self.startAlert
  session?.begin()
 }
 func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
  DispatchQueue.main.async {
   self.msg = messages.map {
    $0.records.map {
     String(decoding: $0.payload, as: UTF8.self)
    }.joined(separator: "\n")
   }.joined(separator: " ")
   self.raw = messages.map {
    $0.records.map {
     "\($0.typeNameFormat) \(String(decoding:$0.type, as: UTF8.self)) \(String(decoding:$0.identifier, as: UTF8.self)) \(String(decoding: $0.payload, as: UTF8.self))"
    }.joined(separator: "\n")
   }.joined(separator: " ")
   DetectDetailsTip.hasDetected = true
   HideNFCTip.notUsingTags = false
   self.tagPositionTip.invalidate(reason: .actionPerformed)
   if self.clothingToFind != nil {
    session.alertMessage = self.endAlert != "" ? self.endAlert : self.findClothing(for: self.msg)
   } else {
    session.alertMessage = self.endAlert != "" ? self.endAlert : self.displayName(for: self.msg)
    if self.firstInvalidates {
     self.detectedClothing = self.detectClothing(for: self.msg)
     self.firstInvalidates = false
    }
   }
   AccessibilityNotification.Announcement(session.alertMessage).post()
  }
 }
 public func readerSessionDidBecomeActive(_ session: NFCNDEFReaderSession) {
 }
 func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
  print("Session did invalidate with error: \(error)")
  if let readerError = error as? NFCReaderError {
   if readerError.errorCode == NFCReaderError.readerSessionInvalidationErrorSessionTimeout.rawValue || readerError.errorCode == NFCReaderError.readerSessionInvalidationErrorUserCanceled.rawValue {
    TagPositionTip.hasTimedOut = true
   }
  }
  self.session = nil
  self.wardrobesToAnalyze = nil
  self.clothingToFind = nil
 }
}

@Observable class NFCWriter: NSObject, NFCNDEFReaderSessionDelegate {
 var startAlert = "Hold your iPhone near the tag."
 var endAlert = ""
 var msg = ""
 var type = "T"
 var session: NFCNDEFReaderSession?
 var playSounds: Bool = false
 var tagPositionTip = TagPositionTip()
 func write() {
  guard NFCNDEFReaderSession.readingAvailable else {
   print("Error")
   return
  }
  session = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
  session?.alertMessage = self.startAlert
  session?.begin()
 }
 func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
 }
 func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
  if tags.count > 1 {
   let retryInterval = DispatchTimeInterval.milliseconds(500)
   session.alertMessage = "Individuato più di un tag. Riprova."
   if self.playSounds {
    Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
   }
   DispatchQueue.global().asyncAfter(deadline: .now() + retryInterval, execute: {
    session.restartPolling()
   })
   return
  }
  let tag = tags.first!
  session.connect(to: tag, completionHandler: { (error: Error?) in
   if nil != error {
    session.alertMessage = "Impossibile connettersi al tag."
    if self.playSounds {
     Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
    }
    session.invalidate()
    return
   }
   tag.queryNDEFStatus(completionHandler: { (ndefStatus: NFCNDEFStatus, capacity: Int, error: Error?) in
    guard error == nil else {
     session.alertMessage = "Impossibile verificare lo stato del tag."
     if self.playSounds {
      Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
     }
     session.invalidate()
     return
    }
    switch ndefStatus {
    case .notSupported:
     session.alertMessage = "Tipo di tag non supportato."
     if self.playSounds {
      Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
     }
     session.invalidate()
    case .readOnly:
     session.alertMessage = "Individuato tag di sola lettura."
     if self.playSounds {
      Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
     }
     session.invalidate()
    case .readWrite:
     let payload: NFCNDEFPayload?
     if self.type == "T" {
      payload = NFCNDEFPayload.init(
       format: .nfcWellKnown,
       type: Data("\(self.type)".utf8),
       identifier: Data(),
       payload: Data("\(self.msg)".utf8)
      )
     } else {
      payload = NFCNDEFPayload.wellKnownTypeURIPayload(string: "\(self.msg)")
     }
     let message = NFCNDEFMessage(records: [payload].compactMap({ $0 }))
     tag.writeNDEF(message, completionHandler: { (error: Error?) in
      if nil != error {
       session.alertMessage = "Scrittura del tag non riuscita: \(error!.localizedDescription)"
       if self.playSounds {
        Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
       }
      } else {
       session.alertMessage = "Tag scritto con successo."
       if self.playSounds {
        Sounds.playSounds(soundfile: "349201__hotlavaman__magic-mallet.wav")
       }
       self.tagPositionTip.invalidate(reason: .actionPerformed)
       HideNFCTip.notUsingTags = false
       Task { await ImmediateTagTip.hasTagged.donate() }
      }
      AccessibilityNotification.Announcement(session.alertMessage).post()
      session.invalidate()
     })
    @unknown default:
     session.alertMessage = "Stato del tag sconosciuto."
     if self.playSounds {
      Sounds.playSounds(soundfile: "521873__opticaillusions__glimmer.wav")
     }
     AccessibilityNotification.Announcement(session.alertMessage).post()
     session.invalidate()
    }
   })
  })
 }
 
 func readerSessionDidBecomeActive(_ session: NFCNDEFReaderSession) {
 }
 
 func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
  print("Session did invalidate with error: \(error)")
  if let readerError = error as? NFCReaderError {
   if readerError.errorCode == NFCReaderError.readerSessionInvalidationErrorSessionTimeout.rawValue || readerError.errorCode == NFCReaderError.readerSessionInvalidationErrorUserCanceled.rawValue {
    TagPositionTip.hasTimedOut = true
   }
  }
  self.session = nil
 }
}

