//
//  MatchesView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 03/06/23.
//

import SwiftUI
import SwiftData

struct MatchesView: View {
 @Query var wardrobes: [Wardrobe]
 var toCompare: [Clothing]
 @State private var joined = ""
 @Environment(Settings.self) var settings
 @State private var selectedClothing: Clothing? = nil
 @State private var orderedMatch: String? = nil
 @State private var showingStyleView = false
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 @State var searchText = ""
 func findMatches(for toCompare: [Clothing]) -> [Clothing] {
  let matchesToCompare: [[UUID]] = toCompare.map { $0.matches }
  var foundIds = [UUID]()
  var found = [Clothing]()
  for matches in matchesToCompare {
   let others = matchesToCompare.filter { $0 != matches }
   for match in matches {
    if others.allSatisfy({ $0.contains(match) }) {
     if !foundIds.contains(match) {
      foundIds.append(match)
     }
    }
   }
  }
  for id in foundIds {
   if let possibleMatch = wardrobes[settings.lastWardrobe].unClothes.first(where: { $0.id == id }) {
    found.append(possibleMatch)
   }
  }
  return found
 }
 @State var currentMatches: [Clothing] = []
 
 var body: some View {
  Group {
   if !currentMatches.isEmpty {
    List {
     ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, filterPredicate: { currentMatches.contains($0) }) { row in
      NavigationLink(row.name, destination: MatchesView(toCompare: toCompare + [row])).id(UUID())
     }
    }
   } else {
    ContentUnavailableView {
     Label("Look completo", systemImage: "sparkles")
      .accessibilityLabel("✨ Look completo")
    } description: {
     Text("Questo abbinamento è perfetto!")
    }
   }
  }
  .navigationTitle(joined)
  .navigationBarTitleDisplayMode(.inline)
  .toolbar {
   ToolbarItem(placement: .topBarLeading) {
    ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
   }
   ToolbarItem(placement: .topBarTrailing) {
    Button(action: {
     showingStyleView = true
    }, label: {
     let match = toCompare.sorted { $0.id < $1.id }.map { $0.id.uuidString }.joined(separator: " + ")
     let currentStyle = wardrobes[settings.lastWardrobe].styles[match, default: MatchInfo()]
     if currentStyle.formalityLevel != .undefined || !currentStyle.styleNotes.isEmpty {
      EmbossedHeaderView(headerTitle: String(localized: "Info"))
     } else {
      Text("Info")
     }
    })
    .accessibilityHint("Tocca due volte per visualizzare o modificare le informazioni su questo abbinamento; tocca due volte e tieni premuto per accedere ai dettagli dei singoli capi.")
    .contextMenu {
     ForEach(toCompare) { item in
      Button("Dettagli \"\(item.name)\"") {
       selectedClothing = item
      }
      if !settings.hideNFCFeatures {
       NFCFindButton(clothingToFind: item, showName: true, detectedDetails: Binding.constant(nil))
      }
     }
    }
   }
  }
  .onChange(of: showingStyleView) {
   if showingStyleView {
    orderedMatch = toCompare.sorted { $0.id < $1.id }.map { $0.id.uuidString }.joined(separator: " + ")
   }
  }
  .sheet(item: $selectedClothing) { item in
   SimpleDetailsView(clothing: item)
  }
  .sheet(isPresented: $showingStyleView) {
   StyleView(showing: $showingStyleView, currentMatch: orderedMatch ?? "Abbinamento sconosciuto")
  }
  .onAppear {
   print("MatchesView appeared")
   currentMatches = findMatches(for: toCompare)
   print("Current matches: ", currentMatches.map { $0.name })
   var names = [String]()
   for i in toCompare {
    names.append(i.name)
   }
   joined = names.joined(separator: " + ")
  }
 }
}

struct StyleView: View {
 @Query var wardrobes: [Wardrobe]
 @Environment(Settings.self) var settings
 @Binding var showing: Bool
 var currentMatch: String
 @State private var showingEditStyleView = false
 @State var title: String = ""
 var currentStyle: MatchInfo {
  return wardrobes[settings.lastWardrobe].styles[currentMatch, default: MatchInfo()]
 }
 @State private var showMoreInfoAlert = false
 
 func makeTitle() -> String {
  let matchIds = currentMatch.components(separatedBy: " + ")
  var matchNames = [String]()
  for id in matchIds {
   if let name = wardrobes[settings.lastWardrobe].unClothes.first(where: { $0.id.uuidString == id })?.name {
    matchNames.append(name)
   }
  }
  return matchNames.joined(separator: " + ")
 }
 
 var body: some View {
  NavigationStack {
   List {
    Section("Livello di formalità") {
     HStack {
      switch currentStyle.formalityLevel {
      case .undefined:
       Text("Non definito")
      case .sports:
       Text("Sportivo")
      case .casual:
       Text("Casual")
      case .formal:
       Text("Formale")
      case .smart:
       Text("Elegante")
      case .ceremonial:
       Text("Da cerimonia")
      }
      Button(action: {
       showMoreInfoAlert = true
      }, label: {
       VStack {
        Image(systemName: "questionmark.square.dashed.ar")
        Text("Altre info")
       }
      })
      .alert("Livello di formalità", isPresented: $showMoreInfoAlert) {
      } message: {
       switch currentStyle.formalityLevel {
       case .undefined:
        Text("Il livello di formalità non è definito.")
       case .sports:
        Text("Il livello \"sportivo\" è adatto a indumenti tecnici come tute o divise sportive.")
       case .casual:
        Text("Il livello \"casual\" è adatto a completi informali da indossare nelle situazioni di tutti i giorni.")
       case .formal:
        Text("Il livello \"formale\" è adatto per abiti che indosseresti in occasioni ufficiali o in una riunione importante.")
       case .smart:
        Text("Il livello \"elegante\" è pensato per gli abiti che indosseresti per una festa raffinata o per una serata.")
       case .ceremonial:
        Text("Il livello \"da cerimonia\" è quello degli abiti per le grandi occasioni.")
       }
      }
     }
    }
    Section("Note sullo stile") {
     if !currentStyle.styleNotes.isEmpty {
      Text(currentStyle.styleNotes)
     } else {
      ContentUnavailableView {
       Text("Nessun'altra informazione")
      } description: {
       Text("Non ci sono altre informazioni sullo stile di questo abbinamento.")
      }
     }
    }
   }
   .onAppear {
    title = makeTitle()
   }
   .navigationTitle("Informazioni stile (\(title))")
   .navigationBarTitleDisplayMode(.inline)
   .toolbar {
    Button(action: {
     showingEditStyleView = true
    }, label: {
     VStack {
      Image(systemName: "pencil")
      Text("Modifica")
     }
    })
   }
   .sheet(isPresented: $showingEditStyleView) {
    EditStyleView(showing: $showingEditStyleView, currentMatch: currentMatch, title: title)
   }
  }
 }
}

struct EditStyleView: View {
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @Environment(\.modelContext) var modelContext
 @Binding var showing: Bool
 var currentMatch: String
 var title: String
 @State private var styleNotes = ""
 @State private var selectedLevel: MatchInfo.FormalityLevel = .undefined
 @Environment(Settings.self) var settings
 @State private var showMoreInfoAlert = false
 var edited: Bool {
  if let currentStyle = wardrobes[settings.lastWardrobe].styles[currentMatch] {
   if selectedLevel != currentStyle.formalityLevel || styleNotes != currentStyle.styleNotes {
    return true
   } else {
    return false
   }
  } else {
   if selectedLevel != .undefined || styleNotes != "" {
    return true
   } else {
    return false
   }
  }
 }
 @FocusState var notesIsFocused: Bool
 var body: some View {
  NavigationStack {
   Form {
    Section(header: HStack {
     Text("Livello di formalità")
     Button(action: {
      showMoreInfoAlert = true
     }, label: {
      VStack {
       Image(systemName: "questionmark.square.dashed.ar")
       Text("Altre info")
      }
     })
    }) {
     Picker("", selection: $selectedLevel) {
      Text("Non definito").tag(MatchInfo.FormalityLevel.undefined)
      Text("Sportivo").tag(MatchInfo.FormalityLevel.sports)
      Text("Casual").tag(MatchInfo.FormalityLevel.casual)
      Text("Formale").tag(MatchInfo.FormalityLevel.formal)
      Text("Elegante").tag(MatchInfo.FormalityLevel.smart)
      Text("Da cerimonia").tag(MatchInfo.FormalityLevel.ceremonial)
     }
     .pickerStyle(.inline)
     .labelsHidden()
     .alert("Livello di formalità", isPresented: $showMoreInfoAlert) {
     } message: {
      Text("""
      Il livello \"sportivo\" è adatto a indumenti tecnici come tute o divise sportive.\n
      Il livello \"casual\" è adatto a completi informali da indossare nelle situazioni di tutti i giorni.\n
      Il livello \"formale\" è adatto per abiti che indosseresti in occasioni ufficiali o in una riunione importante.\n
      Il livello \"elegante\" è pensato per gli abiti che indosseresti per una festa raffinata o per una serata.\n
      Il livello \"da cerimonia\" è quello degli abiti per le grandi occasioni.
      """)
     }
    }
    Section("Note sullo stile") {
     TextField("Note sullo stile", text: $styleNotes, axis: .vertical)
      .focused($notesIsFocused)
    }
   }
   .interactiveDismissDisabled(edited)
   .onAppear {
    if let currentStyle = wardrobes[settings.lastWardrobe].styles[currentMatch] {
     selectedLevel = currentStyle.formalityLevel
     styleNotes = currentStyle.styleNotes
    }
   }
   .navigationTitle("Modifica informazioni stile (\(title))")
   .navigationBarTitleDisplayMode(.inline)
   .toolbar {
    ToolbarItem(placement: .cancellationAction) {
     CancelButton(edited: edited, showing: $showing)
    }
    ToolbarItem(placement: .confirmationAction) {
     Button(action: {
      wardrobes[settings.lastWardrobe].styles[currentMatch] = MatchInfo(formalityLevel: selectedLevel, styleNotes: styleNotes)
      modelContext.insert(wardrobes[settings.lastWardrobe])
      showing = false
     }, label: {
      VStack {
       Image(systemName: "archivebox")
       Text("Salva")
      }
     })
    }
    ToolbarItem(placement: .keyboard) {
     Button(action: {
      notesIsFocused = false
     }, label: {
      VStack {
       Image(systemName: "arrowshape.down")
       Text("Fine")
      }
     })
    }
   }
  }
 }
}

struct SimpleDetailsView: View {
 var clothing: Clothing
 var body: some View {
  NavigationStack {
   List {
    Text("Tipo: \(clothing.type)")
    Text("Colori: \(clothing.unColor.isEmpty ? "Nessuno" : clothing.unColor.map { $0.name }.joined(separator: ", "))")
    Text("Fantasia: \(clothing.patternName.isEmpty ? "Tinta unita" : clothing.patternName)")
    if !clothing.notes.isEmpty {
     Text("Note: \(clothing.notes)")
    }
    if !clothing.clothingDescription.isEmpty {
     Text("Descrizione: \(clothing.clothingDescription)")
    }
   }
   .navigationTitle("Dettagli \(clothing.name)")
   .navigationBarTitleDisplayMode(.inline)
  }
 }
}

