//
//  ColorMatchApp.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 28/03/23.
//

import SwiftUI
import SwiftData
import TipKit

@main
struct ColorMatchApp: App {
 @State var showingAddingView = false
 @State var showingAddingMultipleView = false
 let modelContainer: ModelContainer
 
 init() {
  do {
   modelContainer = try ModelContainer(for: Wardrobe.self, Settings.self, ClothingColor.self)
  // try Tips.resetDatastore()
   try Tips.configure()
   Task { await HideNFCTip.openedApp.donate() }
   print("opened app", HideNFCTip.openedApp.donations.count, "times")
  } catch {
   fatalError("An error occurred during app initialization: \(error.localizedDescription)")
  }
 }
 var body: some Scene {
  WindowGroup {
   ContentView(showingAddingView: $showingAddingView, showingAddingMultipleView: $showingAddingMultipleView)
  }
  .commands {
   CommandGroup(before: CommandGroupPlacement.saveItem) {
    Button("Aggiungi più capi") {
     showingAddingMultipleView = true
    }
   // .keyboardShortcut("p")
   }
   CommandGroup(replacing: CommandGroupPlacement.newItem) {
    Button("Nuovo capo") {
     showingAddingView = true
    }
   // .keyboardShortcut("n")
   }
  }
  .modelContainer(modelContainer)
 }
}



