//
//  AudioModel.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 30/04/24.
//

import Foundation
import AVFoundation

class Sounds {
 static var audioPlayer: AVAudioPlayer?
 static func playSounds(soundfile: String) {
  if let path = Bundle.main.path(forResource: soundfile, ofType: nil) {
   do {
    try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers])
    try AVAudioSession.sharedInstance().setActive(true)
    audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
    audioPlayer?.prepareToPlay()
    audioPlayer?.play()
   } catch {
    print("Error")
   }
  }
 }
}


