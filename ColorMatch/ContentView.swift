//
//  ContentView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 28/03/23.
//

import SwiftUI
import SwiftData
import TipKit
import CloudKitSyncMonitor

struct ContentView: View {
 var hideNFCTip = HideNFCTip()
 var addMultipleTip = AddMultipleTip()
 var detectDetailsTip = DetectDetailsTip()
 var sustainableDeletionTip = SustainableDeletionTip()
 @State var editMode: EditMode = .inactive
 @State var confirmMultipleDeletionAlert = false
 @Environment(\.modelContext) var modelContext
 @Environment(\.accessibilityReduceMotion) var reduceMotion
 @Environment(\.scenePhase) var scenePhase
 @Query var wardrobes: [Wardrobe]
 @State var searchText = ""
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 @State var clothesToDelete = Set<Clothing>()
 @Binding var showingAddingView: Bool
 @Binding var showingAddingMultipleView: Bool
 @State var settings: Settings?
 @State var selectedRandomMatch = [Clothing]()
 @State var path = NavigationPath()
 @State private var randomMatchDialog = false
 @State private var detectedDetails: Clothing? = nil
 @StateObject var syncMonitor = SyncMonitor.shared
 
 var body: some View {
  NavigationStack(path: $path) {
   if let settings {
    if settings.unlocked || !settings.useBiometrics {
     TipView(addMultipleTip, arrowEdge: .top)
      .accessibilityForTip()
     TipView(detectDetailsTip, arrowEdge: .top)
      .accessibilityForTip()
     TipView(hideNFCTip) { action in
      if action.id == "hide-nfc-features" {
       settings.hideNFCFeatures = true
       hideNFCTip.invalidate(reason: .actionPerformed)
      }
     }
     .accessibilityForTip()
     TipView(sustainableDeletionTip)
      .accessibilityForTip()
     Group {
      if !wardrobes[settings.lastWardrobe].unClothes.isEmpty {
       ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, moveEnabled: true, filterPredicate: { wardrobes[settings.lastWardrobe].unClothes.contains($0) }, clothesToDelete: $clothesToDelete) { row in
        ClothingRow(row: row)
       }
       .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always), prompt: "Cerca un capo per nome.")
       .onChange(of: editMode, initial: true) {
        if editMode.isEditing == false {
         clothesToDelete.removeAll()
        }
       }
       .onAppear {
        sort = settings.clothesSorting
        grouping = settings.clothesGrouping
        settings.temporaryDisableShake = false
       }
       .onDisappear {
        settings.temporaryDisableShake = true
       }
       .onShake {
        if settings.shakeForRandomMatch && !settings.temporaryDisableShake && !showingAddingView && !showingAddingMultipleView && !randomMatchDialog {
         randomMatchDialog = true
        } else {
         randomMatchDialog = false
        }
       }
       .confirmationDialog("Premi \"Abbinamento casuale\" o scegli un livello", isPresented: $randomMatchDialog, titleVisibility: .visible) {
        Button("Abbinamento casuale") { selectedRandomMatch = findRandomMatch() }
        Button("Sportivo") { selectedRandomMatch = findRandomMatch(level: .sports) }
         .disabled(!levelIsAvailable(.sports))
        Button("Casual") { selectedRandomMatch = findRandomMatch(level: .casual) }
         .disabled(!levelIsAvailable(.casual))
        Button("Formale") { selectedRandomMatch = findRandomMatch(level: .formal) }
         .disabled(!levelIsAvailable(.formal))
        Button("Elegante") { selectedRandomMatch = findRandomMatch(level: .smart) }
         .disabled(!levelIsAvailable(.smart))
        Button("Da cerimonia") { selectedRandomMatch = findRandomMatch(level: .ceremonial) }
         .disabled(!levelIsAvailable(.ceremonial))
       }
       .onChange(of: detectedDetails) {
        if let detected = detectedDetails {
         path.append(detected)
        }
       }
       .navigationDestination(for: Clothing.self) { selection in
        DetailsView(currentClothing: selection)
       }
       .navigationDestination(for: Int.self) { count in
        if count > 1 {
         MatchesView(toCompare: selectedRandomMatch)
        } else {
         DetailsView(currentClothing: selectedRandomMatch.first!)
        }
       }
      } else {
       ContentUnavailableView {
        Label("Nessun capo", systemImage: "door.sliding.left.hand.open")
       } description: {
        Text("Il guardaroba è vuoto! Puoi aggiungere un capo usando il pulsante \"Aggiungi\" (\(Image(systemName: "plus"))) in alto.")
         .accessibilityLabel("Il guardaroba è vuoto! Puoi aggiungere un capo usando il pulsante \"Aggiungi\" in alto.")
       }
      } // if current wardrobe is empty
     } // Group
     .onChange(of: wardrobes, initial: true) { oldValue, newValue in
      for n in 0..<newValue.count {
       if newValue.filter({ $0 == newValue[n] }).count > 1 {
        Wardrobe.logger.critical("Invalid state: multiple instances of the same wardrobe occur in the collection: \(newValue[n].name), \(newValue.filter({ $0 == newValue[n] }).count) instances.")
        Wardrobe.logger.notice("Attempting to recover old state: inserting \(oldValue[n].name).")
        modelContext.insert(oldValue[n])
        fatalError()
       }
      }
     }
     .onAppear {
      print("ContentView appeared")
      if reduceMotion {
       settings.highlightSelection = false
      }
      if wardrobes.count < 2 {
       settings.speakWardrobeName = false
      }
     }
     .navigationTitle(settings.showWardrobeName ? "Guardaroba (\(wardrobes[settings.lastWardrobe].name))" : "Guardaroba")
     .toolbar {
      ToolbarItem(placement: .topBarTrailing) {
       Button(action: {
        showingAddingView = true
       }, label: {
        VStack {
         Image(systemName: "plus")
         Text("Aggiungi")
        }
       })
       .highPriorityGesture(
        LongPressGesture()
         .onEnded { _ in
          showingAddingMultipleView = true
          addMultipleTip.invalidate(reason: .actionPerformed)
          // let generator = UIImpactFeedbackGenerator(style: .heavy)
          // generator.impactOccurred()
         }
        // .keyboardShortcut("p")
       )
       // .keyboardShortcut("n")
       .accessibilityHint("Tocca due volte per aggiungere un capo; tocca due volte e tieni premuto o usa le azioni rotore per aggiungere più capi.")
       .accessibilityAction(named: "Aggiungi più capi") {
        showingAddingMultipleView = true
        addMultipleTip.invalidate(reason: .actionPerformed)
       }
       .sensoryFeedback(.impact, trigger: showingAddingMultipleView)
      }
      if !settings.hideNFCFeatures {
       ToolbarItem(placement: .topBarLeading) {
        NFCFindButton(clothingToFind: nil, detectedDetails: $detectedDetails, detectDetailsTip: detectDetailsTip)
         .accessibilityHint("Tocca due volte per riconoscere più capi; tocca due volte e tieni premuto o usa le azioni rotore per riconoscere un capo e visualizzarne subito i dettagli.")
       }
      }
      ToolbarItem(placement: .topBarLeading) {
       EditButton()
      }
      ToolbarItemGroup(placement: .secondaryAction) {
       ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
       Menu {
        Button("Abbinamento casuale") { selectedRandomMatch = findRandomMatch() }
         .disabled(wardrobes[settings.lastWardrobe].unClothes.isEmpty)
        Section("Oppure scegli un livello") {
         Button("Sportivo") { selectedRandomMatch = findRandomMatch(level: .sports) }
          .disabled(!levelIsAvailable(.sports))
         Button("Casual") { selectedRandomMatch = findRandomMatch(level: .casual) }
          .disabled(!levelIsAvailable(.casual))
         Button("Formale") { selectedRandomMatch = findRandomMatch(level: .formal) }
          .disabled(!levelIsAvailable(.formal))
         Button("Elegante") { selectedRandomMatch = findRandomMatch(level: .smart) }
          .disabled(!levelIsAvailable(.smart))
         Button("Da cerimonia") { selectedRandomMatch = findRandomMatch(level: .ceremonial) }
          .disabled(!levelIsAvailable(.ceremonial))
        }
       } label: {
        HStack {
         Image(systemName: "die.face.\(Int.random(in: 1...6)).fill")
         Text("Abbinamento casuale")
        }
       }
       .onChange(of: selectedRandomMatch) {
        path.append(selectedRandomMatch.count)
       }
       NavigationLink(destination: WardrobesView()) {
        HStack {
         Image(systemName: "door.sliding.left.hand.closed")
         Text("Elenco guardaroba")
        }
       }
       NavigationLink(destination: PaletteView()) {
        HStack {
         Image(systemName: "paintpalette.fill")
         Text("Tavolozza colori")
        }
       }
       NavigationLink(destination: SettingsView(hideNFCTip: hideNFCTip)) {
        HStack {
         Image(systemName: "gearshape.fill")
         Text("Impostazioni")
        }
       }
       NavigationLink(destination: InfoView()) {
        HStack {
         Image(systemName: "info.bubble")
         Text("Info")
        }
       }
      } // secondary group
      ToolbarItem(placement: .bottomBar) {
       if !editMode.isEditing {
        switch syncMonitor.syncStateSummary {
        case .noNetwork:
         Text("Connessione a Internet non attiva. Le modifiche non saranno sincronizzate finché la connessione non è disponibile.")
        case .accountNotAvailable:
         Text("Sincronizzazione con iCloud non disponibile. Le modifiche non saranno sincronizzate finché non attivi l'accesso ad iCloud.")
        case .error:
         Text("Errore nella sincronizzazione con iCloud.")
        case .notSyncing:
         Text("Sincronizzazione con iCloud sospesa.")
        case .notStarted:
         Text("Sincronizzazione con iCloud non ancora iniziata.")
        case .inProgress:
         Text("Sincronizzo con iCloud...")
        case .succeeded:
         Text("Sincronizzato con iCloud.")
        case .unknown:
         Text("Stato della sincronizzazione con iCloud sconosciuto.")
        }
       } else {
        Button("Elimina \(clothesToDelete.count) capi") {
         confirmMultipleDeletionAlert = true
        }.disabled(clothesToDelete.isEmpty)
         .alert("Elimina capi", isPresented: $confirmMultipleDeletionAlert) {
          Button("Annulla", role: .cancel) { clothesToDelete.removeAll() }
          Button("Elimina", role: .destructive) {
           for item in clothesToDelete {
            wardrobes[settings.lastWardrobe].delete(item, context: modelContext)
            if !wardrobes.contains(where: { $0.unClothes.contains(item) }) {
             SustainableDeletionTip.hasDeleted = true
            }
           }
          }
         } message: {
          Text("Confermi di voler eliminare i capi: \(Array(clothesToDelete).sorted(by: { $0.name < $1.name }).map { "\"\($0.name)\"" }, format: .list(type: .and))?")
         }
       }
      } // bottom
      
      // ToolbarItem(placement: .principal) {
      // VStack {
      // Text("Guardaroba").font(.headline)
      // if settings.showWardrobeName {
      // Text("\(wardrobes[settings.lastWardrobe].name)").font(.subheadline)
      // }
      // }.accessibilityHidden(true)
      // }
      
     } // toolbar
     .environment(\.editMode, $editMode)
    } else {
     Button(action: {
      settings.authenticate()
     }, label: {
      VStack {
       Image(systemName: "lock.fill")
       Text("Sblocca")
      }
     })
     .padding()
    } // if unlocked
   } // if let settings
  } // navigationView
  .onChange(of: scenePhase) {
   if let settings {
    if scenePhase == .active {
     if settings.temporaryDisableShake {
      settings.temporaryDisableShake = false
     }
    } else if scenePhase == .inactive {
     if settings.shakeForRandomMatch {
      settings.temporaryDisableShake = true
     }
    } else if scenePhase == .background {
     settings.unlocked = false
    }
   }
  }
  .onAppear {
   print("navigation stack appeared")
   // load settings
   let settingsRequest = FetchDescriptor<Settings>()
   let settingsData = try? modelContext.fetch(settingsRequest)
   settings = settingsData?.first ?? Settings()
   print("Loaded settings")
   print("settings.lastWardrobe =", settings!.lastWardrobe)
   for w in wardrobes {
    print("- \(w.name)")
   }
   if wardrobes.isEmpty {
    Wardrobe.logger.notice("Using default")
    let defaultWardrobe = Wardrobe(id: UUID(), name: "Default", clothes: [Clothing]())
    modelContext.insert(defaultWardrobe)
   }
   if let settings {
    if settings.lastWardrobe > wardrobes.count-1 {
     Wardrobe.logger.notice("Falling back to 0.")
     settings.lastWardrobe = 0
    }
    if settings.useBiometrics && !settings.unlocked {
     print("Authenticating...")
     settings.authenticate()
    }
   }
  }
  .sheet(isPresented: $showingAddingView) {
   ClothingEditorView(showing: $showingAddingView)
  }
  .sheet(isPresented: $showingAddingMultipleView) {
   AddingMultipleView(showingAddingMultipleView: $showingAddingMultipleView)
  }
  .environment(settings)
 } // body
 func findRandomMatch(level: MatchInfo.FormalityLevel? = nil) -> [Clothing] {
  switch level {
  case .undefined:
   fallthrough
  case .none:
   let start = wardrobes[settings!.lastWardrobe].unClothes.randomElement()!
   if !start.matches.isEmpty {
    let randomId = start.matches.randomElement()!
    let randomMatch = wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id == randomId })!
    return [start, randomMatch]
   } else {
    return [start]
   }
  case .sports:
   let randomMatch = wardrobes[settings!.lastWardrobe].styles.filter { key, value in
    value.formalityLevel == .sports
   }.keys.randomElement()!
   return randomMatch.components(separatedBy: " + ").compactMap({ match in
    wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id.uuidString == match }) })
  case .casual:
   let randomMatch = wardrobes[settings!.lastWardrobe].styles.filter { key, value in
    value.formalityLevel == .casual
   }.keys.randomElement()!
   return randomMatch.components(separatedBy: " + ").compactMap({ match in
    wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id.uuidString == match }) })
  case .formal:
   let randomMatch = wardrobes[settings!.lastWardrobe].styles.filter { key, value in
    value.formalityLevel == .formal
   }.keys.randomElement()!
   return randomMatch.components(separatedBy: " + ").compactMap({ match in
    wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id.uuidString == match }) })
  case .smart:
   let randomMatch = wardrobes[settings!.lastWardrobe].styles.filter { key, value in
    value.formalityLevel == .smart
   }.keys.randomElement()!
   return randomMatch.components(separatedBy: " + ").compactMap({ match in
    wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id.uuidString == match }) })
  case .ceremonial:
   let randomMatch = wardrobes[settings!.lastWardrobe].styles.filter { key, value in
    value.formalityLevel == .ceremonial
   }.keys.randomElement()!
   return randomMatch.components(separatedBy: " + ").compactMap({ match in
    wardrobes[settings!.lastWardrobe].unClothes.first(where: { $0.id.uuidString == match }) })
  }
 }
 func levelIsAvailable(_ level: MatchInfo.FormalityLevel) -> Bool {
  return wardrobes[settings!.lastWardrobe].styles.values.contains { $0.formalityLevel == level }
 }
} // struct

// Da https://www.hackingwithswift.com/quick-start/swiftui/how-to-detect-shake-gestures
// The notification we'll send when a shake gesture happens.
extension UIDevice {
 static let deviceDidShakeNotification = Notification.Name(rawValue: "deviceDidShakeNotification")
}

//  Override the default behavior of shake gestures to send our notification instead.
extension UIWindow {
 open override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
  if motion == .motionShake {
   NotificationCenter.default.post(name: UIDevice.deviceDidShakeNotification, object: nil)
  }
 }
}

// A view modifier that detects shaking and calls a function of our choosing.
struct DeviceShakeViewModifier: ViewModifier {
 let action: () -> Void
 func body(content: Content) -> some View {
  content
   .onAppear()
   .onReceive(NotificationCenter.default.publisher(for: UIDevice.deviceDidShakeNotification)) { _ in
    action()
   }
 }
}

// A View extension to make the modifier easier to use.
extension View {
 func onShake(perform action: @escaping () -> Void) -> some View {
  self.modifier(DeviceShakeViewModifier(action: action))
 }
}




