//
//  WardrobesView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 21/06/23.
//

import SwiftUI
import SwiftData

struct WardrobesView: View {
 @Environment(\.modelContext) var modelContext
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @Environment(Settings.self) var settings
 @State private var showingAddingWardrobeView = false
 @State private var confirmDeletionAlert = false
 @State private var toDelete: Wardrobe? = nil
 @State private var confirmEmptyAlert = false
 func delete(_ wardrobe: Wardrobe) {
  let n = wardrobes.firstIndex(of: wardrobe)!
  if n == settings.lastWardrobe {
   settings.lastWardrobe = 0
   modelContext.insert(settings)
  }
  modelContext.delete(wardrobe)
 }
 func empty(_ wardrobe: Wardrobe) {
  modelContext.insert(wardrobe)
  wardrobe.clothes?.removeAll()
  wardrobe.styles.removeAll()
  try! modelContext.save()
 }
 var body: some View {
  @Bindable var settings = settings
  List {
   Section("Guardaroba corrente") {
    Picker("Guardaroba corrente", selection: $settings.lastWardrobe) {
     ForEach(wardrobes) { item in
      Text(item.name).tag(wardrobes.firstIndex(of: item) ?? 0)
     }
    }
    .sensoryFeedback(.selection, trigger: settings.lastWardrobe)
    .pickerStyle(.wheel)
    .onChange(of: wardrobes) { oldValue, newValue in
     for n in 0..<newValue.count {
      if newValue.filter({ $0 == newValue[n] }).count > 1 {
       Wardrobe.logger.critical("Invalid state: multiple instances of the same wardrobe occur in the collection.")
       Wardrobe.logger.notice("Attempting to recover old state.")
       modelContext.insert(oldValue[n])
       fatalError("Invalid state: multiple instances of the same wardrobe occur in the collection.")
      }
     }
    }
   }
   Section(header: HStack {
    Text("Guardaroba (\(wardrobes.count))")
    Button(action: {
     showingAddingWardrobeView = true
    }, label: {
     VStack {
      Image(systemName: "plus")
      Text("Aggiungi")
     }
    })
   }) {
    ForEach(wardrobes) { item in
     WardrobeRow(wardrobe: item)
      .onAppear {
       print("wardrobe row appeared")
      }
      .swipeActions {
       Button("Elimina") {
        toDelete = item
        confirmDeletionAlert = true
       }
       .disabled(wardrobes.count == 1)
       Button("Svuota") {
        toDelete = item
        confirmEmptyAlert = true
       }
      }
    }
    .alert("Elimina guardaroba", isPresented: $confirmDeletionAlert) {
     Button("Annulla", role: .cancel) { }
     Button("Elimina", role: .destructive) {
      if settings.lastWardrobe >= wardrobes.count-1 {
       settings.lastWardrobe -= 1
       modelContext.insert(settings)
      }
      delete(toDelete!)
      print("sustainable:", !wardrobes.contains(where: { $0.unClothes.contains(toDelete?.clothes ?? []) }))
      if !wardrobes.contains(where: { $0.unClothes.contains(toDelete?.clothes ?? []) }) {
       SustainableDeletionTip.hasDeleted = true
      }
     }
    } message: {
     Text("Confermi di voler eliminare il guardaroba \"\(toDelete?.name ?? "???")\"?")
    }
    .alert("Svuota guardaroba", isPresented: $confirmEmptyAlert) {
     Button("Annulla", role: .cancel) { }
     Button("Svuota", role: .destructive) {
      empty(toDelete!)
      print("sustainable:", !wardrobes.contains(where: { $0.unClothes.contains(toDelete?.clothes ?? []) }))
      if !wardrobes.contains(where: { $0.unClothes.contains(toDelete?.clothes ?? []) }) {
       SustainableDeletionTip.hasDeleted = true
      }
     }
    } message: {
     Text("Confermi di voler svuotare il guardaroba \"\(toDelete?.name ?? "???")\"? Il guardaroba rimarrà nell'elenco, ma tutti i capi al suo interno verranno eliminati.")
    }
    .onChange(of: showingAddingWardrobeView, initial: true) {
     print("showing adding wardrobe:", showingAddingWardrobeView)
    }
   }
  }
  .navigationTitle("Elenco guardaroba")
  .navigationBarTitleDisplayMode(.inline)
  .sheet(isPresented: $showingAddingWardrobeView) {
   WardrobeEditorView(showing: $showingAddingWardrobeView)
    .onAppear {
     print("Adding sheet appeared.")
    }
  }
  .onAppear {
   print("wardrobes view appeared.")
  }
 }
}


