//
//  AddingView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 14/04/23.
//

import SwiftUI
import SwiftData
import TipKit

struct AddingMultipleView: View {
 var automaticTypeTip = AutomaticTypeTip()
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @Binding var showingAddingMultipleView: Bool
 @State private var multipleString = ""
 @State private var selectedMultipleType = String(localized: "Non impostato")
 @State private var temporaryAutomaticType = false
 @State private var selectedMultipleMatches = [Clothing]()
 @State private var crossMatches = false
 @State private var washingInfo = WashingInfo()
 @FocusState private var namesIsFocused: Bool
 var edited: Bool {
  return !multipleString.isEmpty || selectedMultipleType != String(localized: "Non impostato") || !selectedMultipleMatches.isEmpty || washingInfo != WashingInfo()
 }
 @State var searchText = ""
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 
 var body: some View {
  NavigationStack {
   Form {
    Text("Inserisci i nomi dei capi da aggiungere, uno per riga. Puoi anche selezionare un tipo e degli abbinamenti per i nuovi capi. Potrai modificare le altre informazioni in seguito.")
    Section("Seleziona un tipo per i capi da aggiungere") {
     Picker("Seleziona tipo", selection: $selectedMultipleType) {
      ForEach(settings.types, id: \.self) {
       Text($0)
      }
     }
     .disabled(temporaryAutomaticType)
     .onChange(of: temporaryAutomaticType) {
      selectedMultipleType = String(localized: "Non impostato")
      if temporaryAutomaticType {
       automaticTypeTip.invalidate(reason: .actionPerformed)
      }
     }
     TipView(automaticTypeTip, arrowEdge: .bottom) { action in
      if action.id == "turn-on-automaticType" {
       settings.automaticTypes = true
      }
     }
      .accessibilityForTip()
     Toggle("Inserisci automaticamente tipo", isOn: $temporaryAutomaticType)
      .onChange(of: settings.automaticTypes, initial: true) {
       temporaryAutomaticType = settings.automaticTypes
       if settings.automaticTypes {
        automaticTypeTip.invalidate(reason: .actionPerformed)
       }
      }
    }
    Section("Nomi") {
     TextEditor(text: $multipleString)
      .focused($namesIsFocused)
     Toggle("Questi capi si abbinano tra loro.", isOn: $crossMatches)
    }
    DisclosureGroup(content: {
     Section("Programma") {
      Picker("", selection: $washingInfo.washingProgram) {
       Text("Non specificato").tag(WashingInfo.WashingProgram.unspecified)
       Text("Chiari").tag(WashingInfo.WashingProgram.lights)
       Text("Chiari (delicati)").tag(WashingInfo.WashingProgram.lightDelicates)
       Text("Chiari (altro programma)").tag(WashingInfo.WashingProgram.lightsOther)
       Text("Scuri").tag(WashingInfo.WashingProgram.darks)
       Text("Scuri (delicati)").tag(WashingInfo.WashingProgram.darkDelicates)
       Text("Scuri (altro programma)").tag(WashingInfo.WashingProgram.darksOther)
      }
      .pickerStyle(.inline)
      .labelsHidden()
      if washingInfo.washingProgram == .darksOther || washingInfo.washingProgram == .lightsOther {
       TextField("Specifica il programma", text: $washingInfo.programName)
      }
     }
     Section("Note sul lavaggio") {
      TextField(text: $washingInfo.washingNotes, axis: .vertical) {
       Text("Note (ad es. temperatura, programma di asciugatura...)")
        .fixedSize()
      }
     }
    }, label: {
     if washingInfo.washingProgram != .unspecified || !washingInfo.washingNotes.isEmpty {
      WateryHeaderView(headerTitle: String(localized: "Informazioni sul lavaggio"))
     } else {
      Text("Informazioni sul lavaggio")
     }
    })
    Section(header: HStack {
     Text("Seleziona abbinamenti (\(selectedMultipleMatches.count) selezionati)")
     ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
    }) {
     Text("Gli abbinamenti che scegli verranno aggiunti a tutti i capi.")
     if wardrobes[settings.lastWardrobe].unClothes.isEmpty {
      ContentUnavailableView {
       Text("Nessun capo")
      } description: {
       Text("Non ci sono capi da mostrare.")
      }
     } else {
      ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, filterPredicate: { wardrobes[settings.lastWardrobe].unClothes.contains($0) }, selectAllEnabled: true, selectedItems: $selectedMultipleMatches) { row in
       SelectionButton(selectedItems: $selectedMultipleMatches, rowItem: row)
      }
     }
    }
   }
   .interactiveDismissDisabled(multipleString != "")
   .navigationTitle("Aggiungi più capi")
   .navigationBarTitleDisplayMode(.inline)
   .toolbar {
    ToolbarItem(placement: .cancellationAction) {
     CancelButton(edited: edited, showing: $showingAddingMultipleView)
    }
    ToolbarItem(placement: .confirmationAction) {
     Button("Aggiungi") {
      wardrobes[settings.lastWardrobe].addMultiple(from: multipleString, type: selectedMultipleType, washingInfo: washingInfo, matches: selectedMultipleMatches.map({ $0.id }), crossMatches: crossMatches, automaticTypes: temporaryAutomaticType, types: &settings.types, sort: settings.sortTypes, context: modelContext)
      modelContext.insert(wardrobes[settings.lastWardrobe])
      showingAddingMultipleView = false
     }
     .disabled(multipleString.isEmpty)
    }
    ToolbarItem(placement: .keyboard) {
     Button(action: {
      namesIsFocused = false
     }, label: {
      VStack {
       Image(systemName: "arrowshape.down")
       Text("Fine")
      }
     })
    }
   }
   .onChange(of: showingAddingMultipleView) {
    settings.temporaryDisableShake = showingAddingMultipleView
   }
  }
 }
}

