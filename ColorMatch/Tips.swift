//
//  Tips.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 26/11/23.
//

import TipKit

struct AddMultipleTip: Tip {
 var title: Text {
  Text("Aggiungi più capi")
 }
 var message: Text? {
  Text("Tieni premuto il pulsante \"Aggiungi\" (\(Image(systemName: "plus"))) per aggiungere più capi.")
   .accessibilityLabel("Tieni premuto il pulsante \"Aggiungi\" per aggiungere più capi.")
 }
 var image: Image? {
  Image(systemName: "plus")
 }
}

struct DetectDetailsTip: Tip {
 @Parameter
 static var hasDetected: Bool = false
 var title: Text {
  Text("Vedi subito i dettagli di un capo")
 }
 var message: Text? {
  Text("Tieni premuto il pulsante \"Riconosci\" (\(Image(systemName: "waveform.badge.magnifyingglass"))) per visualizzare subito i dettagli del capo appena riconosciuto.")
   .accessibilityLabel("Tieni premuto il pulsante \"Riconosci\" per visualizzare subito i dettagli del capo appena riconosciuto.")
 }
 var image: Image? {
  Image(systemName: "waveform.badge.magnifyingglass")
 }
 var rules: [Rule] {
  [
   #Rule(Self.$hasDetected) {
    $0 == true
   }
  ]
 }
}

struct AutomaticTypeTip: Tip {
 var title: Text {
  Text("Inserimento automatico tipo")
 }
 var message: Text? {
  Text("Lascia che sia l'app a impostare per te il tipo in base al nome del capo. Ad esempio, se scrivi \"Pantaloni verdi\", il tipo sarà automaticamente impostato su \"pantaloni\". Se quest'ultimo non è incluso nell'elenco dei tipi presente nelle impostazioni, verrà aggiunto automaticamente. Puoi attivare o disattivare questa funzionalità nelle impostazioni, o modificarla temporaneamente da qui.")
 }
 var actions: [Action] {
  Action(id: "turn-on-automaticType", title: "Attiva inserimento automatico tipo")
 }
}

struct JustANameTip: Tip {
 var title: Text {
  Text("Basta un nome")
 }
 var message: Text? {
  Text("Ogni capo deve avere un nome, ma gli altri campi sono facoltativi. Inserisci le informazioni che ti servono, tante o poche.")
 }
}

struct ImmediateTagTip: Tip {
 static let hasTagged = Event(id: "hasTagged")
 var title: Text {
  Text("Associa subito a un tag NFC")
 }
 var message: Text? {
  Text("Tieni premuto il pulsante \"Salva\" (\(Image(systemName: "archivebox"))) per associare subito il nuovo capo a un tag.")
   .accessibilityLabel("Tieni premuto il pulsante \"Salva\" per associare subito il nuovo capo a un tag.")
 }
 var image: Image? {
  Image(systemName: "archivebox")
 }
 var rules: [Rule] {
  [
   #Rule(Self.hasTagged) {
    $0.donations.count >= 3
   }
  ]
 }
}

struct IdTip: Tip {
 var title: Text {
  Text("Distingui i capi usando i tag NFC")
 }
 var message: Text? {
  Text("Questo ID è ciò che ti consente di associare ogni capo a un tag NFC per poterlo riconoscere facilmente. Utilizza il pulsante \"Scrivi tag\" (\(Image(systemName: "phone.and.waveform"))) per associare subito un tag, oppure il pulsante \"Copia\" (\(Image(systemName: "clipboard"))) per copiare l'ID e utilizzarlo in un'app esterna.")
   .accessibilityLabel("Questo ID è ciò che ti consente di associare ogni capo a un tag NFC per poterlo riconoscere facilmente. Utilizza il pulsante \"Scrivi tag\" per associare subito un tag, oppure il pulsante \"Copia\" per copiare l'ID e utilizzarlo in un'app esterna.")
 }
 var image: Image? {
  Image(systemName: "phone.and.waveform")
 }
}

struct HideNFCTip: Tip {
 @Parameter
 static var notUsingTags: Bool = true
 static let openedApp = Event(id: "openedApp")
 var title: Text {
  Text("Nascondi le funzionalità NFC")
 }
 var message: Text? {
  Text("Non utilizzi i tag? Puoi nascondere le relative funzionalità nelle impostazioni.")
 }
 var actions: [Action] {
  Action(id: "hide-nfc-features", title: "Nascondi funzionalità NFC")
 }
 var rules: [Rule] {
  [
   #Rule(Self.$notUsingTags) {
    $0 == true
   },
   #Rule(Self.openedApp) {
    $0.donations.count >= 10
   }
  ]
 }
}

struct TagPositionTip: Tip {
 @Parameter
 static var hasTimedOut: Bool = false
 var title: Text {
  Text("Qualcosa non ha funzionato?")
 }
 var message: Text? {
  Text("Prova ad avvicinare il tag alla parte superiore del dispositivo, accanto alla fotocamera.")
 }
 var rules: [Rule] {
  [
   #Rule(Self.$hasTimedOut) {
    $0 == true
   }
  ]
 }
 var options: [Option] {
  Tips.MaxDisplayCount(1)
 }
}

struct SustainableDeletionTip: Tip {
 @Parameter
 static var hasDeleted: Bool = false
 var title: Text {
  Text("Non porti più un capo?")
 }
 var message: Text? {
  Text("Non buttarlo via! Ricordati che puoi sempre donarlo a chi ne ha bisogno.")
 }
 var image: Image? {
  Image(systemName: "gift.circle.fill")
 }
 var rules: [Rule] {
  [
   #Rule(Self.$hasDeleted) {
    $0 == true
   }
  ]
 }
}


struct AccessibilityForTip: ViewModifier {
 func body(content: Content) -> some View {
  content
   .accessibilityElement(children: .contain)
   .accessibilityLabel("Suggerimento")
 }
}

extension View {
 func accessibilityForTip() -> some View {
  modifier(AccessibilityForTip())
 }
}


