//
//  Custom Views.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 18/07/23.
//

import SwiftUI
import SwiftData
import TipKit
import NaturalLanguage
import CoreNFC
import CoreML

// MARK: SelectAllButton
struct SelectAllButton: View {
 @Binding var selectedItems: [Clothing]
 let allSelectableItems: [Clothing]
 var body: some View {
  Button(selectedItems.contains(allSelectableItems) ? "Deseleziona tutto" : "Seleziona tutto") {
   if selectedItems.contains(allSelectableItems) {
    selectedItems = selectedItems.filter { !allSelectableItems.contains($0) }
   } else {
    selectedItems += allSelectableItems
    selectedItems.removeDuplicates()
   }
  }
 }
}

// MARK: SelectionButton
struct SelectionButton<I: Named & Identifiable & Equatable>: View {
 @Environment(Settings.self) var settings
 @Binding var selectedItems: [I]
 @State var rowItem: I
 @State var phase = 0.0
 var body: some View {
  ZStack {
   Button(selectedItems.contains(where: { $0 == rowItem }) ? "\(Image(systemName: "checkmark")) \(rowItem.name)" : "\(rowItem.name)") {
    if selectedItems.contains(where: { $0 == rowItem }) {
     let ind = selectedItems.firstIndex(of: rowItem)!
     selectedItems.remove(at: ind)
    } else {
     selectedItems.append(rowItem)
    }
   }
   .padding()
   if selectedItems.contains(where: { $0 == rowItem }) && settings.highlightSelection {
    Rectangle()
     .strokeBorder(Color.primary.gradient, style: StrokeStyle(lineWidth: 5, dash: [10], dashPhase: phase))
     .transition(.diamonds(size: 20))
     .onAppear {
      withAnimation(.linear.repeatForever(autoreverses: false)) {
       phase -= 5
      }
     }
   }
  }
 }
}

// MARK: CancelButton
struct CancelButton: View {
 var edited: Bool
 @State var confirmationAlert = false
 @Binding var showing: Bool
 var body: some View {
  Button("Annulla") {
   if edited {
    confirmationAlert = true
   } else {
    showing = false
   }
  }
  .alert("Annulla modifiche", isPresented: $confirmationAlert) {
   Button("Conferma", role: .destructive) {
    showing = false
   }
   Button("Annulla", role: .cancel) { }
  } message: {
   Text("Hai inserito dei dati in questa schermata. Confermi di volerla chiudere? I dati inseriti andranno persi.")
  }
 }
}

// MARK: ClothesListView
struct ClothesListView<Content: View>: View {
 let filterPredicate: (Clothing) -> Bool
 var moveEnabled: Bool
 var selectAllEnabled: Bool
 @Binding var selectedItems: [Clothing]
 let content: (Clothing) -> Content
 @Binding var sort: [SortDescriptor<Clothing>]
 @Binding var searchText: String
 @Binding var grouping: ClothesGroupingCriteria?
 @Binding var clothesToDelete: Set<Clothing>
 var body: some View {
  if grouping == nil {
   ClothesNotSortedListView(sort: sort, searchText: searchText, moveEnabled: moveEnabled, filterPredicate: filterPredicate, clothesToDelete: $clothesToDelete) { row in
    content(row)
   }
  } else {
   ClothesSortedListView(sort: sort, searchText: searchText, grouping: $grouping, moveEnabled: moveEnabled, filterPredicate: filterPredicate, selectAllEnabled: selectAllEnabled, selectedItems: $selectedItems, clothesToDelete: $clothesToDelete) { row in
    content(row)
   }
  }
 }
 init(sort: Binding<[SortDescriptor<Clothing>]>, searchText: Binding<String>, grouping: Binding<ClothesGroupingCriteria?>, moveEnabled: Bool = false, filterPredicate: @escaping (Clothing) -> Bool, selectAllEnabled: Bool = false, selectedItems: Binding<[Clothing]> = Binding.constant([Clothing]()), clothesToDelete: Binding<Set<Clothing>> = Binding.constant(Set<Clothing>()), content: @escaping (Clothing) -> Content) {
  self._sort = sort
  self._searchText = searchText
  self._grouping = grouping
  self.moveEnabled = moveEnabled
  self.filterPredicate = filterPredicate
  self.selectAllEnabled = selectAllEnabled
  self._selectedItems = selectedItems
  self._clothesToDelete = clothesToDelete
  self.content = content
 }
}

// MARK: ClothesSortedListView
enum ClothesGroupingCriteria: Codable {
 case name, type, color, temperature
}
struct ClothesSortedListView<Content: View>: View {
 @ViewBuilder let content: (Clothing) -> Content
 var moveEnabled: Bool = false
 var filterPredicate: (Clothing) -> Bool // = { $0.wardrobes?.contains(wardrobes[settings.lastWardrobe]) ?? true }
 var selectAllEnabled: Bool
 @Binding var selectedItems: [Clothing]
 @Environment(\.editMode) var editMode
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @Query var clothes: [Clothing]
 @Binding var currentCriteria: ClothesGroupingCriteria?
 @State var currentClothes = [Clothing]()
 @State var headings: [String]? = nil
 @Binding var clothesToDelete: Set<Clothing>
 func shouldInclude(_ clothing: Clothing, under currentHeading: String) -> Bool {
  switch currentCriteria {
  case .name: return String(clothing.name.first!) == currentHeading
  case .type: return clothing.type == currentHeading
  case .color: return (clothing.dominantColor?.name ?? clothing.color?.first?.name ?? String(localized: "Nessuno")) == currentHeading
  case .temperature: return (clothing.dominantColor?.temperatureString ?? clothing.color?.first?.temperatureString ?? String(localized: "Sconosciuto")) == currentHeading
  case .none: return false
  }
 }
 
 func getHeadings() {
  if let criteria = currentCriteria {
   switch criteria {
   case .name: headings = currentClothes.compactMap { String($0.name.first!) }.removingDuplicates()
   case .type: headings = settings.types.filter { type in currentClothes.contains(where: { $0.type == type }) }
   case .color: headings = currentClothes.map { $0.dominantColor?.name ?? $0.color?.first?.name ?? String(localized: "Nessuno") }.removingDuplicates()
   case .temperature: headings = currentClothes.map { $0.dominantColor?.temperatureString ?? $0.color?.first?.temperatureString ?? String(localized: "Sconosciuto") }.removingDuplicates()
   }
  } else {
   headings = nil
  }
 }
 
 @ViewBuilder var groupedList: some View {
  if currentCriteria != nil && headings != nil {
   ForEach(headings!, id:\.self) { currentHeading in
    Section(header: HStack {
     Text(currentHeading)
     if selectAllEnabled {
      SelectAllButton(selectedItems: $selectedItems, allSelectableItems: currentClothes.filter { shouldInclude($0, under: currentHeading)})
     }
    }) {
     ForEach(currentClothes) { row in
      if shouldInclude(row, under: currentHeading) {
       content(row)
      }
     }
     .onMove(perform: moveEnabled ? {
      wardrobes[settings.lastWardrobe].reorder(source: $0, destination: $1)
      modelContext.insert(wardrobes[settings.lastWardrobe])
     } : nil)
    }
   }.listStyle(.grouped)
  }
 }
 
 var body: some View {
  Group {
   if moveEnabled && editMode?.wrappedValue.isEditing == true {
    List(selection: $clothesToDelete) {
     groupedList
    }
   } else if moveEnabled && editMode?.wrappedValue.isEditing == false {
    List {
     groupedList
    }
   } else {
    ScrollView {
     groupedList
    }
   }
  }
  .onChange(of: clothes, initial: true) {
   currentClothes = clothes.filter { filterPredicate($0) }
   getHeadings()
  }
  .onChange(of: currentCriteria, initial: true) {
   getHeadings()
  }
 }
 init(sort: [SortDescriptor<Clothing>], searchText: String, grouping: Binding<ClothesGroupingCriteria?>, moveEnabled: Bool = false, filterPredicate: @escaping (Clothing) -> Bool, selectAllEnabled: Bool = false, selectedItems: Binding<[Clothing]> = Binding.constant([Clothing]()), clothesToDelete: Binding<Set<Clothing>> = Binding.constant(Set<Clothing>()), content: @escaping (Clothing) -> Content) {
  _currentCriteria = grouping
  _clothes = Query(filter: #Predicate<Clothing> { clothing in
   if searchText.isEmpty {
    return true
   } else {
    return clothing.name.localizedStandardContains(searchText)
   }
  }, sort: sort)
  self.moveEnabled = moveEnabled
  self.filterPredicate = filterPredicate
  self.selectAllEnabled = selectAllEnabled
  self._selectedItems = selectedItems
  self._clothesToDelete = clothesToDelete
  self.content = content
 }
}

// MARK: ClothesSortGroupMenu
struct ClothesSortGroupMenu: View {
 @Environment(Settings.self) var settings
 @Binding var sort: [SortDescriptor<Clothing>]
 @Binding var grouping: ClothesGroupingCriteria?
 var body: some View {
  Menu("Ordina e raggruppa") {
   Section {
    Text("Ordina capi per:")
     .accessibilityAddTraits([.isStaticText, .isHeader])
    Picker("", selection: $sort) {
     Text("Nome (a - z)").tag([SortDescriptor(\Clothing.name, comparator: .localizedStandard)])
     Text("Colore dominante (a - z)").tag([SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)])
     Text("Colore dominante (caldo - freddo)").tag([SortDescriptor(\Clothing.dominantColor?.temperatureInt, order: .reverse), SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)])
     Text("Colore dominante (freddo - caldo)").tag([SortDescriptor(\Clothing.dominantColor?.temperatureInt), SortDescriptor(\Clothing.dominantColor?.name, comparator: .localizedStandard), SortDescriptor(\Clothing.name, comparator: .localizedStandard)])
     Text("Ordinamento personalizzato").tag([SortDescriptor(\Clothing.order)])
    }.pickerStyle(.inline)
   }
   Section {
    Text("Raggruppa capi per:")
     .accessibilityAddTraits([.isStaticText, .isHeader])
    Picker("", selection: $grouping) {
     Text("Nessuno").tag(Optional<ClothesGroupingCriteria>.none)
     Text("Nome").tag(ClothesGroupingCriteria.name as ClothesGroupingCriteria?)
     Text("Tipo").tag(ClothesGroupingCriteria.type as ClothesGroupingCriteria?)
     Text("Colore dominante").tag(ClothesGroupingCriteria.color as ClothesGroupingCriteria?)
     Text("Temperatura colore dominante").tag(ClothesGroupingCriteria.temperature as ClothesGroupingCriteria?)
    }.pickerStyle(.inline)
   }
  }
  .onAppear {
   sort = settings.clothesSorting
   grouping = settings.clothesGrouping
  }
 }
}

// MARK: ClothesNotSortedListView
struct ClothesNotSortedListView<Content: View>: View {
 @ViewBuilder let content: (Clothing) -> Content
 var moveEnabled: Bool = false
 var filterPredicate: (Clothing) -> Bool // = { $0.wardrobes?.contains(wardrobes[settings.lastWardrobe]) ?? true }
 @Environment(\.editMode) var editMode
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @Query var clothes: [Clothing]
 @State var currentClothes = [Clothing]()
 @Binding var clothesToDelete: Set<Clothing>
 @ViewBuilder var listContent: some View {
  ForEach(currentClothes) { row in
   content(row)
  }
  .onMove(perform: moveEnabled ? {
   wardrobes[settings.lastWardrobe].reorder(source: $0, destination: $1)
   modelContext.insert(wardrobes[settings.lastWardrobe])
  } : nil)
 }
 
 var body: some View {
  Group {
   if moveEnabled && editMode?.wrappedValue.isEditing == true {
    List(selection: $clothesToDelete) {
     listContent
    }
   } else if moveEnabled && editMode?.wrappedValue.isEditing == false {
    List {
     listContent
    }
   } else {
    ScrollView {
     listContent
    }
   }
  }
  .onChange(of: clothes, initial: true) {
   currentClothes = clothes.filter { filterPredicate($0) }
  }
 }
 init(sort: [SortDescriptor<Clothing>], searchText: String, moveEnabled: Bool = false, filterPredicate: @escaping (Clothing) -> Bool, clothesToDelete: Binding<Set<Clothing>> = Binding.constant(Set<Clothing>()), content: @escaping (Clothing) -> Content) {
  _clothes = Query(filter: #Predicate<Clothing> { clothing in
   if searchText.isEmpty {
    return true
   } else {
    return clothing.name.localizedStandardContains(searchText)
   }
  }, sort: sort)
  self.moveEnabled = moveEnabled
  self.filterPredicate = filterPredicate
  self._clothesToDelete = clothesToDelete
  self.content = content
 }
}

extension Array where Element: Hashable {
 func removingDuplicates() -> [Element] {
  var addedDict = [Element: Bool]()
  return filter {
   addedDict.updateValue(true, forKey: $0) == nil
  }
 }
 mutating func removeDuplicates() {
  self = self.removingDuplicates()
 }
}

// MARK: ClothingForm
struct ClothingForm: View {
 enum FocusedField: CaseIterable {
  case name, color, pattern, notes, description, otherProgram, washingNotes
 }
 
 @FocusState var focusedField: FocusedField?
 @Environment(Settings.self) var settings
 var immediateTagTip: ImmediateTagTip
 var automaticTypeTip = AutomaticTypeTip()
 var justANameTip: JustANameTip
 @Query var wardrobes: [Wardrobe]
 @Query var colors: [ClothingColor]
 var currentId: UUID? = nil
 @Binding var name: String
 @Binding var pattern: String
 @Binding var color: String
 @Binding var selectedType: String
 @Binding var selectedMatches: [Clothing]
 @Binding var notes: String
 @Binding var description: String
 @Binding var washingInfo: WashingInfo
 @Binding var image: UIImage?
 @Binding var temporaryAutomaticType: Bool
 @State private var searchText = ""
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 @State var autofill = false
 @State var suggestMatches = false
 @State var suggestedMatches = [Clothing]()
 @State private var showPhotoOptions = false
 @State private var source = UIImagePickerController.SourceType.camera
 @State private var showingImagePicker = false
 @Binding var addedColors: [ClothingColor]
 @State var showPaletteChooser = false
 @State var autofillSuccessful = false
 
 var body: some View {
  Form {
   Section(header: HStack {
    Text("Foto")
    Button(image == nil ? "Aggiungi foto" : "Modifica foto") {
     showPhotoOptions = true
    }
    if image != nil {
     Button("Elimina foto") {
      image = nil
     }
    }
   }) {
    if let image {
     Image(uiImage: image)
      .resizable()
      .scaledToFit()
    } else {
     ContentUnavailableView("Nessuna foto", systemImage: "photo")
    }
   }
   if currentId == nil {
    TipView(immediateTagTip, arrowEdge: .top)
     .accessibilityForTip()
   }
   Section("Informazioni") {
    Picker("Seleziona tipo di capo", selection: $selectedType) {
     ForEach(settings.types, id:\.self) {
      Text($0)
     }
    }
    .disabled(temporaryAutomaticType)
    .onChange(of: temporaryAutomaticType, initial: true) {
     if currentId == nil {
      selectedType = String(localized: "Non impostato")
     } else {
      temporaryAutomaticType = false
     }
     if temporaryAutomaticType {
      automaticTypeTip.invalidate(reason: .actionPerformed)
     }
    }
    if currentId == nil {
     TipView(automaticTypeTip, arrowEdge: .bottom) { action in
      if action.id == "turn-on-automaticType" {
       settings.automaticTypes = true
      }
     }
     .accessibilityForTip()
     Toggle("Inserisci automaticamente tipo", isOn: $temporaryAutomaticType)
      .onChange(of: settings.automaticTypes, initial: true) {
       temporaryAutomaticType = settings.automaticTypes
      }
    }
    TipView(justANameTip, arrowEdge: .bottom)
     .accessibilityForTip()
    TextField("Nome (es. Blue jeans)", text: $name)
     .focused($focusedField, equals: .name)
    HStack {
     TextField("Scrivi uno o più colori", text: $color)
      .focused($focusedField, equals: .color)
      .onChange(of: color) {
       if !color.isEmpty {
        addedColors = color.components(separatedBy: ", ").compactMap { name in
         if !colors.contains(where: { $0.name == name.capitalized }) {
          return ClothingColor(name: name.capitalized)
         } else {
          return nil
         }
        }
       } else {
        addedColors = []
       }
      }
     Button("Apri tavolozza", systemImage: "paintpalette") { showPaletteChooser = true }
    }
    if !addedColors.isEmpty {
     DisclosureGroup("Informazioni colori") {
      ForEach($addedColors) { $color in
       Section("Temperatura \"\(color.name)\"") {
        Picker("", selection: $color.temperature) {
         Text("Sconosciuta").tag(Optional<ClothingColor.Temperature>.none)
         Text("Fredda").tag(ClothingColor.Temperature.cold as ClothingColor.Temperature?)
         Text("Neutra").tag(ClothingColor.Temperature.neutral as ClothingColor.Temperature?)
         Text("Calda").tag(ClothingColor.Temperature.warm as ClothingColor.Temperature?)
        }
        .pickerStyle(.segmented)
       }
      }
     }
    }
    TextField("Definisci la fantasia, se presente", text: $pattern)
     .focused($focusedField, equals: .pattern)
    Button(!autofill ? "Inserisci automaticamente" : "Cancella") { autofill.toggle() }
     .disabled(name.isEmpty)
    Text(!autofill ? "Inserisci automaticamente i colori e la fantasia interpretando il nome con l'intelligenza artificiale presente sul dispositivo." : autofillSuccessful ? "Cancella il riempimento proposto." : "Non è stato possibile formulare un suggerimento.")
     .onChange(of: autofill) {
      if autofill {
       autofillSuccessful = autofillFields()
      } else {
       if focusedField == .color || focusedField == .pattern || !autofillSuccessful {
        return
       } else {
        color = ""
        pattern = ""
        autofillSuccessful = false
       }
      }
     }
     .onChange(of: name) {
      if autofill {
       autofill = false
      }
     }
     .onChange(of: color) {
      if autofill && focusedField == .color {
       autofill = false
      }
     }
     .onChange(of: pattern) {
      if autofill && focusedField == .pattern {
       autofill = false
      }
     }
    TextField(text: $notes, axis: .vertical) {
     Text("Note (es. caratteristiche per il riconoscimento...)")
      .fixedSize()
    }
    .focused($focusedField, equals: .notes)
    TextField("Descrizione", text: $description, axis: .vertical)
     .focused($focusedField, equals: .description)
   }
   DisclosureGroup(content: {
    Section("Programma") {
     Picker("", selection: $washingInfo.washingProgram) {
      Text("Non specificato").tag(WashingInfo.WashingProgram.unspecified)
      Text("Chiari").tag(WashingInfo.WashingProgram.lights)
      Text("Chiari (delicati)").tag(WashingInfo.WashingProgram.lightDelicates)
      Text("Chiari (altro programma)").tag(WashingInfo.WashingProgram.lightsOther)
      Text("Scuri").tag(WashingInfo.WashingProgram.darks)
      Text("Scuri (delicati)").tag(WashingInfo.WashingProgram.darkDelicates)
      Text("Scuri (altro programma)").tag(WashingInfo.WashingProgram.darksOther)
     }
     .pickerStyle(.inline)
     .labelsHidden()
     if washingInfo.washingProgram == .darksOther || washingInfo.washingProgram == .lightsOther {
      TextField("Specifica il programma", text: $washingInfo.programName)
       .focused($focusedField, equals: .otherProgram)
     }
    }
    Section("Note sul lavaggio") {
     TextField(text: $washingInfo.washingNotes, axis: .vertical) {
      Text("Note (ad es. temperatura, programma di asciugatura...)")
       .fixedSize()
     }
     .focused($focusedField, equals: .washingNotes)
    }
   }, label: {
    if washingInfo.washingProgram != .unspecified || !washingInfo.washingNotes.isEmpty {
     WateryHeaderView(headerTitle: String(localized: "Informazioni sul lavaggio"))
    } else {
     Text("Informazioni sul lavaggio")
    }
   })
   Section(header: HStack {
    Text("Seleziona abbinamenti (\(selectedMatches.count) selezionati)")
    ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
   }) {
    if !wardrobes[settings.lastWardrobe].unClothes.isEmpty || wardrobes[settings.lastWardrobe].unClothes.count == 1 && currentId != nil {
     if !searchResults.isEmpty {
      Button(!suggestMatches ? "Suggerisci abbinamenti" : "Cancella") { suggestMatches.toggle() }
      .disabled(color.isEmpty)
      .onChange(of: suggestMatches) {
       var allColors = color.components(separatedBy: ", ").flatMap { name in colors.first(where: { $0.name == name }) ?? addedColors.first(where: { $0.name == name }) }
       if let firstColor = allColors.first {
        let matchableColors = firstColor.getMatchableColors(wardrobes: wardrobes)
        suggestedMatches = wardrobes[settings.lastWardrobe].unClothes.filter { ($0.id != currentId) && (!$0.unColor.isEmpty) && (matchableColors.contains($0.dominantColor ?? $0.unColor.first!)) }
       }
       if suggestMatches {
        selectedMatches += suggestedMatches
        selectedMatches.removeDuplicates()
       } else {
        selectedMatches = selectedMatches.filter { !suggestedMatches.contains($0) }
       }
      }
      .onChange(of: color) {
       suggestMatches = false
      }
      Text(!suggestMatches ? "Seleziona automaticamente i capi che potrebbero abbinarsi in base ai rispettivi colori principali." : !suggestedMatches.isEmpty ? "Cancella la selezione proposta." : "Non sono stati trovati abbinamenti per il colore principale.")
      ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, filterPredicate: { wardrobes[settings.lastWardrobe].unClothes.contains($0) && $0.id != currentId }, selectAllEnabled: true, selectedItems: $selectedMatches) { row in
       SelectionButton(selectedItems: $selectedMatches, rowItem: row)
      }
     } else {
      ContentUnavailableView.search(text: searchText)
     }
    } else {
     ContentUnavailableView {
      Text("Nessun capo")
     } description: {
      Text("Non ci sono\(currentId != nil ? " altri " : " ")capi da mostrare.")
     }
    }
   }
  }
  .confirmationDialog("Scegli come aggiungere la foto", isPresented: $showPhotoOptions, titleVisibility: .visible) {
   Button("Scatta foto") {
    source = .camera
    showingImagePicker = true
   }
   Button("Seleziona dalla libreria") {
    source = .photoLibrary
    showingImagePicker = true
   }
  }
  .sheet(isPresented: $showingImagePicker) {
   CameraView(photo: $image, source: source)
  }
  .sheet(isPresented: $showPaletteChooser) {
   PaletteChooserView(showing: $showPaletteChooser, selectedColors: $color)
  }
  .toolbar {
   ToolbarItemGroup(placement: .keyboard) {
    Button(action: {
     let index = FocusedField.allCases.firstIndex(of: focusedField!)!
     focusedField = FocusedField.allCases[index - 1]
    }, label: {
     VStack {
      Image(systemName: "arrowshape.backward")
      Text("Indietro")
     }
    })
    .disabled(focusedField == FocusedField.allCases.first!)
    Button(action: {
     focusedField = nil
    }, label: {
     VStack {
      Image(systemName: "arrowshape.down")
      Text("Fine")
     }
    })
    Button(action: {
     let index = FocusedField.allCases.firstIndex(of: focusedField!)!
     focusedField = FocusedField.allCases[index + 1]
    }, label: {
     VStack {
      Image(systemName: "arrowshape.forward")
      Text("Avanti")
     }
    })
    .disabled(focusedField == FocusedField.allCases.last!)
   }
  }
  .searchable(text: $searchText, prompt: "Cerca un abbinamento.")
 }
 var searchResults: [Clothing] {
  if searchText.isEmpty {
   return wardrobes[settings.lastWardrobe].unClothes.filter { $0.id != currentId }
  } else {
   return wardrobes[settings.lastWardrobe].unClothes.filter { $0.id != currentId && $0.name.contains(searchText) }
  }
 }
 func autofillFields() -> Bool {
  var result: Bool
  let configuration = MLModelConfiguration()
  guard let model = try? ColorAndPatternTagger_2(configuration: configuration) else {
   return false
  }
  if let prediction = try? model.prediction(text: name) {
   var colors = [String]()
   var patterns = [String]()
   print("prediction details:")
   print(prediction.labels)
   print(prediction.tokens)
   if !prediction.labels.contains(where: { $0 == "COLOR" || $0 == "PATTERN" || $0 == "SHADE" }) {
    result = false
   } else {
    result = true
    for n in 0...prediction.labels.count-1 {
     if prediction.labels[n] == "COLOR" {
      let lemmatizedTokens = prediction.tokens.joined(separator: " ").lemmatized().components(separatedBy: " ")
      print("lemmatized tokens:", prediction.tokens.joined(separator: " ").lemmatized(), prediction.tokens.joined(separator: " ").components(separatedBy: " "))
      var color = colors.isEmpty ? lemmatizedTokens[n].capitalized : lemmatizedTokens[n]
      if n != prediction.labels.count-1 && prediction.labels[n+1] == "SHADE" {
       color += " \(lemmatizedTokens[n+1])"
      }
      colors.append(color)
     }
     if prediction.labels[n] == "PATTERN" {
      patterns.append(patterns.isEmpty ? prediction.tokens[n].capitalized : prediction.tokens[n])
     }
    }
   }
   if !colors.isEmpty {
    color = colors.joined(separator: ", ")
   }
   if !patterns.isEmpty {
    pattern = patterns.joined(separator: " ")
   }
  } else {
   result = false
  }
  return result
 }
 
 private struct CameraView: UIViewControllerRepresentable {
  class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   var parent: CameraView
   init(_ parent: CameraView) {
    self.parent = parent
   }
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    picker.dismiss(animated: true)
    guard let image = info[.editedImage] as? UIImage else {
     print("No image found")
     return
    }
    self.parent.photo = image
   }
  }
  @Binding var photo: UIImage?
  var source: UIImagePickerController.SourceType
  func makeUIViewController(context: Context) -> UIImagePickerController {
   let vc = UIImagePickerController()
   vc.sourceType = source
   vc.allowsEditing = true
   vc.delegate = context.coordinator
   return vc
  }
  func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
  }
  func makeCoordinator() -> Coordinator {
   Coordinator(self)
  }
 }
 
 private struct PaletteChooserView: View {
  @Binding var showing: Bool
  @Query var colors: [ClothingColor]
  @Binding var selectedColors: String
  @State var currentSelection = [ClothingColor]()
  @State var allColors = [String]()
  var colorNames: [String] {
   return ((selectedColors.isEmpty ? [] : selectedColors.components(separatedBy: ", ").map { $0.capitalized }) + currentSelection.map { $0.name }).removingDuplicates()
  }
  @State var orderedColors: [String]? = nil
  
  var body: some View {
   NavigationStack {
    Group {
     if !colors.isEmpty {
      List(colors) { color in
       SelectionButton(selectedItems: $currentSelection, rowItem: color)
      }
     } else {
      ContentUnavailableView {
       Label("Nessun colore", systemImage: "paintpalette")
      } description: {
       Text("Utilizza la funzione \"Tavolozza\" (accessibile dal menù della schermata principale) per aggiungere un colore alla tavolozza in modo da poterlo inserire facilmente nelle informazioni dei capi. In alternativa, utilizza il campo \"Colore\" quando aggiungi o modifichi un capo per inserire rapidamente nuovi colori nella tavolozza.")
      }
     }
    }
    .onChange(of: colorNames, initial: true) {
     allColors = colorNames
     print("allColors =", allColors)
    }
    .onAppear {
     currentSelection = selectedColors.components(separatedBy: ", ").compactMap { name in
      colors.first(where: { $0.name == name.capitalized })
     }
    }
    .navigationTitle("Seleziona dalla tavolozza")
    .navigationBarTitleDisplayMode(.inline)
    .toolbar {
     ToolbarItem(placement: .navigationBarTrailing) {
      NavigationLink("Riordina", destination: ColorReorderingView(orderedColors: $orderedColors, currentOrder: allColors))
     }
     ToolbarItem(placement: .confirmationAction) {
      Button("Fatto") {
       if orderedColors == nil {
        if selectedColors.isEmpty {
         selectedColors = currentSelection.map { $0.name }.joined(separator: ", ")
        } else {
         var result = [String]()
         for c in allColors {
          if !colors.contains(where: { $0.name == c.capitalized }) {
           result.append(c)
          } else if colors.contains(where: { $0.name == c.capitalized }) && currentSelection.contains(where: { $0.name == c.capitalized }) {
           result.append(c)
          } else if colors.contains(where: { $0.name == c.capitalized }) && !currentSelection.contains(where: { $0.name == c.capitalized }) {
           continue
          }
         }
         selectedColors = result.joined(separator: ", ")
        }
       } else {
        selectedColors = orderedColors!.joined(separator: ", ")
       }
       showing = false
      }
     }
    }
   }
  }
  private struct ColorReorderingView: View {
   @Environment(\.dismiss) var dismiss
   @Binding var orderedColors: [String]?
   @State var currentOrder: [String]
   var body: some View {
    List {
     ForEach(currentOrder, id: \.self) { name in
      Text(name)
     }
     .onMove(perform: {
      currentOrder.move(fromOffsets: $0, toOffset: $1)
     })
    }
    .navigationTitle("Riordina colori")
    .navigationBarTitleDisplayMode(.inline)
    .toolbar {
     EditButton()
     Button("Fatto") {
      orderedColors = currentOrder
      dismiss()
     }
    }
   }
  }
 }
}

extension String {
 func lemmatized() -> String {
  let tagger = NLTagger(tagSchemes: [.lemma])
  tagger.string = self
  var result = [String]()
  tagger.enumerateTags(in: self.startIndex..<self.endIndex, unit: .word, scheme: .lemma) { tag, tokenRange in
   // let stemForm = tag?.rawValue ?? String(self[tokenRange])
   if let stemWord = tag?.rawValue {
    result.append(stemWord)
   } else {
    print("Could not lemmatize \(String(self[tokenRange])).")
    result.append(String(self[tokenRange]))
   }
   // result.append(stemForm)
   return true
  }
  return result.joined()
 }
}

// MARK: WardrobeForm
struct WardrobeForm: View {
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @Binding var name: String
 @Binding var selectedClothes: [Clothing]
 @Binding var selectedWardrobes: [Wardrobe]
 @Environment(Settings.self) var settings
 @FocusState var nameIsFocused: Bool
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 @State var searchText = ""
 var body: some View {
  Form {
   TextField("Nome", text: $name)
    .focused($nameIsFocused)
   Section(header: HStack {
    Text("Copia capi da altri guardaroba (\(selectedClothes.count) selezionati)")
    ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
   }) {
    Text("Seleziona i capi da copiare. Se selezioni capi che si abbinano tra loro, questi abbinamenti verranno configurati automaticamente.")
    ForEach(wardrobes) { wardrobe in
     DisclosureGroup(content: {
      if !wardrobe.unClothes.isEmpty {
       SelectAllButton(selectedItems: $selectedClothes, allSelectableItems: wardrobe.unClothes)
       ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, filterPredicate: { wardrobe.unClothes.contains($0) }, selectAllEnabled: true, selectedItems: $selectedClothes) { row in
        SelectionButton(selectedItems: $selectedClothes, rowItem: row)
       }
      } else {
       ContentUnavailableView {
        Text("Nessun capo")
       } description: {
        Text("Questo guardaroba è vuoto.")
       }
      }
     }, label: {
      Text(wardrobe.name)
       .badge(settings.showClothesCount && !settings.showWardrobeIndicator || wardrobe != wardrobes[settings.lastWardrobe] ? Text("\(wardrobe.unClothes.count) capi") : nil)
       .badge(settings.showWardrobeIndicator && !settings.showClothesCount && wardrobe == wardrobes[settings.lastWardrobe] ? Text("Attuale") : nil)
       .badge(settings.showClothesCount && settings.showWardrobeIndicator && wardrobe == wardrobes[settings.lastWardrobe] ? Text("Attuale, \(wardrobe.unClothes.count) capi") : nil)
     })
    }
    .onChange(of: selectedClothes) {
     for clothing in selectedClothes {
      selectedWardrobes = wardrobes.filter { $0.unClothes.contains(clothing) }.removingDuplicates()
     }
    }
   }
  }
  .toolbar {
   ToolbarItem(placement: .keyboard) {
    Button(action: {
     nameIsFocused = false
    }, label: {
     VStack {
      Image(systemName: "arrowshape.down")
      Text("Fine")
     }
    })
   }
  }
 }
}

// MARK: ClothingRow
struct ClothingRow: View {
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 let row: Clothing
 @State private var toDelete: Clothing? = nil
 @State private var confirmDeletionAlert = false
 
 var body: some View {
  NavigationLink(row.name, value: row)
   .swipeActions {
    Button("Elimina") {
     toDelete = row
     print("toDelete =", toDelete?.name)
     confirmDeletionAlert = true
    }
   }
   .alert("Elimina capo", isPresented: $confirmDeletionAlert, presenting: toDelete) { item in
    Button("Annulla", role: .cancel) { }
    Button("Elimina", role: .destructive) {
     wardrobes[settings.lastWardrobe].delete(item, context: modelContext)
     print(wardrobes[settings.lastWardrobe].unClothes.map { $0.name })
    // modelContext.delete(item)
    // modelContext.insert(wardrobes[settings.lastWardrobe])
     if !wardrobes.contains(where: { $0.unClothes.contains(item) }) {
      SustainableDeletionTip.hasDeleted = true
     }
    }
   } message: { item in
    Text("Confermi di voler eliminare il capo \"\(item.name)\"?")
   }
 }
}

// MARK: WardrobeRow
struct WardrobeRow: View {
 @Environment(Settings.self) var settings
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @State var wardrobe: Wardrobe
 @State private var showingEditingWardrobeView = false
 var body: some View {
  Button(wardrobe.name) {
   showingEditingWardrobeView = true
  }
  .badge(settings.showClothesCount && !settings.showWardrobeIndicator || wardrobe != wardrobes[settings.lastWardrobe] ? Text("\(wardrobe.unClothes.count) capi") : nil)
  .badge(settings.showWardrobeIndicator && !settings.showClothesCount && wardrobe == wardrobes[settings.lastWardrobe] ? Text("Attuale") : nil)
  .badge(settings.showClothesCount && settings.showWardrobeIndicator && wardrobe == wardrobes[settings.lastWardrobe] ? Text("Attuale, \(wardrobe.unClothes.count) capi") : nil)
  .sheet(isPresented: $showingEditingWardrobeView) {
   WardrobeEditorView(showing: $showingEditingWardrobeView, selectedWardrobe: wardrobe)
    .onAppear {
     print("Editing sheet appeared.")
    }
  }
 }
}

// MARK: NFCFindButton
struct NFCFindButton: View {
 @Query var wardrobes: [Wardrobe]
 @Environment(Settings.self) var settings
 var clothingToFind: Clothing?
 @State var detailReader = NFCReader()
 @State private var NFCUnavailableAlert = false
 var showName = false
 @Binding var detectedDetails: Clothing?
 var detectDetailsTip: DetectDetailsTip? = nil
 var body: some View {
  detailReader.startAlert = "Avvicina il telefono al tag sul capo \(clothingToFind == nil ? "che vuoi riconoscere" : "\"\(clothingToFind!.name)\"")."
  detailReader.speakWardrobeName = settings.speakWardrobeName
  detailReader.playSounds = settings.playSounds
  detailReader.wardrobesToAnalyze = wardrobes
  return Button(action: {
   if !NFCReaderSession.readingAvailable {
    NFCUnavailableAlert = true
   } else {
    if let clothingToFind {
     detailReader.clothingToFind = clothingToFind
    }
    detailReader.continuousRead()
   }
  }, label: {
   if !showName {
    VStack {
     if clothingToFind == nil {
      Image(systemName: "waveform.badge.magnifyingglass")
     } else {
      Image(systemName: "waveform.and.magnifyingglass")
     }
     Text(clothingToFind != nil ? "Individua" : "Riconosci")
    }
   } else {
    Text("Individua \"\(clothingToFind?.name ?? "")\"")
   }
  })
  .highPriorityGesture(
   LongPressGesture()
    .onEnded { _ in
     if clothingToFind == nil {
      detailReader.firstInvalidates = true
      detailReader.read()
      detectDetailsTip?.invalidate(reason: .actionPerformed)
     }
    })
  .popoverTip(detailReader.tagPositionTip)
  .onChange(of: detailReader.detectedClothing) { oldValue, newValue in
   if oldValue == nil && newValue != nil {
    if let detected = detailReader.detectedClothing {
     detectedDetails = detected
     detailReader.detectedClothing = nil
    } else {
     detectedDetails = nil
    }
   }
  }
  .alert("NFC non disponibile", isPresented: $NFCUnavailableAlert) {
  } message: {
   Text("Il dispositivo non supporta la tecnologia NFC.")
  }
 }
}

// MARK: WateryHeaderView
struct WateryHeaderView: View {
 var headerTitle: String
 @State private var startTime = Date.now
 var body: some View {
  TimelineView(.animation) { timeline in
   let elapsedTime = startTime.distance(to: timeline.date)
   Text(headerTitle)
    .background(.background)
    .drawingGroup()
    .visualEffect { content, proxy in
     content
      .distortionEffect(
       ShaderLibrary.water(
        .float2(proxy.size),
        .float(elapsedTime),
        .float(3),
        .float(3),
        .float(10)
       ),
       maxSampleOffset: .zero
      )
    }
  }
 }
}

// MARK: EmbossedHeaderView
struct EmbossedHeaderView: View {
 var headerTitle: String
 @State private var embossAmount = 10.0
 var body: some View {
  Text(headerTitle)
   .layerEffect(
    ShaderLibrary.emboss(
     .float(embossAmount)
    ),
    maxSampleOffset: .zero
   )
 }
}





