//
//  PaletteView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 06/05/24.
//

import SwiftUI
import SwiftData

// MARK: PaletteView
struct PaletteView: View {
 @Environment(\.modelContext) var modelContext
 @Query var colors: [ClothingColor]
 @Query var wardrobes: [Wardrobe]
 @State var newColor: ClothingColor? = nil
 @State var searchText = ""
 @State var sort = [SortDescriptor(\ClothingColor.name, comparator: .localizedStandard)]
 @State var grouping: ColorsSortedListView.GroupingCriteria? = nil
 
 var body: some View {
  Group {
   if !colors.isEmpty {
    Group {
     if grouping == nil {
      ColorsNotSortedListView(sort: sort, searchText: searchText)
     } else {
      ColorsSortedListView(sort: sort, searchText: searchText, grouping: $grouping)
     }
    }
    .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always), prompt: "Cerca un colore.")
   } else {
    ContentUnavailableView {
     Label("Nessun colore", systemImage: "paintpalette")
    } description: {
     Text("Premi il pulsante \"Aggiungi\" (\(Image(systemName: "plus"))) per aggiungere un colore alla tavolozza in modo da poterlo inserire facilmente nelle informazioni dei capi.")
      .accessibilityLabel("Premi il pulsante \"Aggiungi\" per aggiungere un colore alla tavolozza in modo da poterlo inserire facilmente nelle informazioni dei capi.")
    }
   }
  }
  .toolbar {
   ToolbarItemGroup(placement: .topBarLeading) {
    Menu("Ordina e raggruppa") {
     Section {
      Text("Ordina colori per:")
      Picker("", selection: $sort) {
       Text("Nome (a - z)").tag([SortDescriptor(\ClothingColor.name, comparator: .localizedStandard)])
       Text("Temperatura (caldo - freddo)").tag([SortDescriptor(\ClothingColor.temperatureInt, order: .reverse), SortDescriptor(\ClothingColor.name, comparator: .localizedStandard)])
       Text("Temperatura (freddo - caldo)").tag([SortDescriptor(\ClothingColor.temperatureInt), SortDescriptor(\ClothingColor.name, comparator: .localizedStandard)])
      }.pickerStyle(.inline)
     }
     Section {
      Text("Raggruppa colori per:")
      Picker("", selection: $grouping) {
       Text("Nessuno").tag(Optional<ColorsSortedListView.GroupingCriteria>.none)
       Text("Nome").tag(ColorsSortedListView.GroupingCriteria.name as ColorsSortedListView.GroupingCriteria?)
       Text("Temperatura").tag(ColorsSortedListView.GroupingCriteria.temperature as ColorsSortedListView.GroupingCriteria?)
      }.pickerStyle(.inline)
     }
    }
   }
   ToolbarItem(placement: .primaryAction) {
    Button(action: {
     newColor = ClothingColor()
    }, label: {
     VStack {
      Image(systemName: "plus")
      Text("Aggiungi")
     }
    })
   }
  }
  .navigationTitle("Tavolozza colori")
  .navigationBarTitleDisplayMode(.inline)
  .sheet(item: $newColor) {
   ColorEditorView(color: $0, title: "Nuovo colore")
  }
 }
}

// MARK: ColorDetailsView
struct ColorDetailsView: View {
 @Query var wardrobes: [Wardrobe]
 var color: ClothingColor
 @State var editingColor: ClothingColor? = nil
 @State var matchableColors = [ClothingColor]()
 
 var body: some View {
  List {
   Text("Temperatura: \(color.temperatureString)")
   Section("Colori abbinabili (\(matchableColors.count))") {
    ForEach(matchableColors) { color in
     HStack {
      Text(color.name)
      if let temperature = color.temperature {
       switch temperature {
       case .cold: Text("Freddo")
       case .neutral: Text("Neutro")
       case .warm: Text("Caldo")
       }
      }
     }
     .accessibilityElement(children: .combine)
    }
   }
  }
  .onAppear {
   matchableColors = color.getMatchableColors(wardrobes: wardrobes)
  }
  .navigationTitle("Dettagli \(color.name)")
  .navigationBarTitleDisplayMode(.inline)
  .toolbar {
   Button(action: {
    editingColor = color
   }, label: {
    VStack {
     Image(systemName: "pencil")
     Text("Modifica")
    }
   })
  }
  .sheet(item: $editingColor) { color in
   ColorEditorView(color: color, title: "Modifica colore (\(color.name))")
  }
 }
}

// MARK: ColorsSortedListView
struct ColorsSortedListView: View {
 enum GroupingCriteria {
  case name, temperature
 }
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @Query var colors: [ClothingColor]
 @State private var colorToDelete: ClothingColor? = nil
 @State private var confirmDeletionAlert = false
 @Binding var currentCriteria: GroupingCriteria?
 var headings: [String] {
  if let criteria = currentCriteria {
   switch criteria {
   case .name: return colors.map { String($0.name.first!) }.removingDuplicates()
   case .temperature: return colors.map { $0.temperatureString }.removingDuplicates()
   }
  } else {
   return []
  }
 }
 func shouldInclude(_ color: ClothingColor, under currentHeading: String) -> Bool {
  switch currentCriteria {
  case .name: return String(color.name.first!) == currentHeading
  case .temperature: return color.temperatureString == currentHeading
  case .none: return false
  }
 }
 
 var body: some View {
  if currentCriteria != nil {
   List {
    ForEach(headings, id:\.self) { currentHeading in
     Section(currentHeading) {
      ForEach(colors) { color in
       if shouldInclude(color, under: currentHeading) {
        NavigationLink(destination: ColorDetailsView(color: color)) {
         HStack {
          Text(color.name)
          if let temperature = color.temperature {
           switch temperature {
           case .cold: Text("Freddo")
           case .neutral: Text("Neutro")
           case .warm: Text("Caldo")
           }
          }
         }
         .accessibilityElement(children: .combine)
        }
        .swipeActions {
         Button("Elimina") {
          colorToDelete = color
          confirmDeletionAlert = true
         }
        }
       }
      }
     }
    }
   }
   .alert("Elimina colore", isPresented: $confirmDeletionAlert, presenting: colorToDelete) { color in
    Button("Annulla", role: .cancel) { }
    Button("Elimina", role: .destructive) {
     for w in wardrobes {
      for c in w.unClothes {
       if c.unColor.contains(color) {
        let i = c.unColor.firstIndex(of: color)!
        c.color?.remove(at: i)
       }
      }
     }
     modelContext.delete(color)
    }
   } message: { color in
    Text("Confermi di voler eliminare il colore \"\(color.name)\"? Il colore verrà rimosso anche dai capi che lo contengono.")
   }
  }
 }
 init(sort: [SortDescriptor<ClothingColor>], searchText: String, grouping: Binding<GroupingCriteria?>) {
  _currentCriteria = grouping
  _colors = Query(filter: #Predicate<ClothingColor> { color in
   if searchText.isEmpty {
    return true
   } else {
    return color.name.localizedStandardContains(searchText)
   }
  }, sort: sort)
 }
}

// MARK: ColorsNotSortedListView
struct ColorsNotSortedListView: View {
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @Query var colors: [ClothingColor]
 @State private var colorToDelete: ClothingColor? = nil
 @State private var confirmDeletionAlert = false
 
 var body: some View {
  List {
   ForEach(colors) { color in
    NavigationLink(destination: ColorDetailsView(color: color)) {
     HStack {
      Text(color.name)
      if let temperature = color.temperature {
       switch temperature {
       case .cold: Text("Freddo")
       case .neutral: Text("Neutro")
       case .warm: Text("Caldo")
       }
      }
     }
     .accessibilityElement(children: .combine)
    }
    .swipeActions {
     Button("Elimina") {
      colorToDelete = color
      confirmDeletionAlert = true
     }
    }
   }
  }
  .alert("Elimina colore", isPresented: $confirmDeletionAlert, presenting: colorToDelete) { color in
   Button("Annulla", role: .cancel) { }
   Button("Elimina", role: .destructive) {
    for w in wardrobes {
     for c in w.unClothes {
      if c.unColor.contains(color) {
       let i = c.unColor.firstIndex(of: color)!
       c.color?.remove(at: i)
      }
     }
    }
    modelContext.delete(color)
   }
  } message: { color in
   Text("Confermi di voler eliminare il colore \"\(color.name)\"? Il colore verrà rimosso anche dai capi che lo contengono.")
  }
 }
 init(sort: [SortDescriptor<ClothingColor>], searchText: String) {
  _colors = Query(filter: #Predicate<ClothingColor> { color in
   if searchText.isEmpty {
    return true
   } else {
    return color.name.localizedStandardContains(searchText)
   }
  }, sort: sort)
 }
}

// MARK: ColorEditorView
struct ColorEditorView: View {
 @Environment(\.modelContext) var modelContext
 @Environment(\.dismiss) var dismiss
 @Query var colors: [ClothingColor]
 @Bindable var color: ClothingColor
 let title: String
 @State var showing = true
 @State var colorExistsAlert = false
 var edited: Bool {
  return !color.name.isEmpty || color.temperature != nil
 }
 
 var body: some View {
  NavigationStack {
   Form {
    TextField("Nome", text: $color.name)
    Section("Temperatura") {
     Picker("", selection: $color.temperature) {
      Text("Sconosciuta").tag(Optional<ClothingColor.Temperature>.none)
      Text("Fredda").tag(ClothingColor.Temperature.cold as ClothingColor.Temperature?)
      Text("Neutra").tag(ClothingColor.Temperature.neutral as ClothingColor.Temperature?)
      Text("Calda").tag(ClothingColor.Temperature.warm as ClothingColor.Temperature?)
     }.pickerStyle(.segmented)
      .onChange(of: color.temperature, initial: true) {
       color.temperatureInt = color.temperature?.rawValue ?? 4
      }
    }
   }
   .onAppear {
    modelContext.autosaveEnabled = false
   }
   .onDisappear {
    modelContext.autosaveEnabled = true
   }
   .onChange(of: showing) {
    if !showing {
     dismiss()
    }
   }
   .alert("Colore già esistente", isPresented: $colorExistsAlert) {
   } message: {
    Text("Il colore \"\(color.name)\" esiste già. Prova con un nome diverso.")
   }
   .navigationTitle(title)
   .navigationBarTitleDisplayMode(.inline)
   .toolbar {
    ToolbarItem(placement: .cancellationAction) {
     CancelButton(edited: edited, showing: $showing)
    }
    ToolbarItem(placement: .confirmationAction) {
     Button(action: {
      if colors.contains(where: { $0.id != color.id && $0.name == color.name }) {
       colorExistsAlert = true
      } else {
       modelContext.insert(color)
       try! modelContext.save()
       dismiss()
      }
     }, label: {
      VStack {
       Image(systemName: "archivebox")
       Text("Salva")
      }
     })
     .disabled(color.name.isEmpty)
    }
   }
  }
 }
}


