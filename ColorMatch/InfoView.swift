//
//  InfoView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 17/10/23.
//

import SwiftUI
import SwiftUIMailView

struct InfoView: View {
 @State private var mailData = ComposeMailData(subject: "Wardrobe Wizard feedback", recipients: ["laura.tosetto.98@icloud.com"], message: "", attachments: [])
 @State private var sendingMail = false
 var appVersion: String? {
  return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
 }
 var body: some View {
  List {
   Text("Versione: \(appVersion ?? "Sconosciuta")")
   Text("Sviluppata da Laura Tosetto.")
    .font(.footnote)
   HStack {
    Text("Per info, feedback e supporto:")
    Button("Contattami!") {
     sendingMail = true
    }
    .disabled(!MailView.canSendMail)
    .sheet(isPresented: $sendingMail) {
     MailView(data: $mailData) { result in
      print(result)
     }
    }
   }
   HStack {
    Text("Sito web:")
    Link("www.wardrobewizard.app", destination: URL(string: "http://www.wardrobewizard.app")!)
   }
   Section("Riconoscimenti") {
    DisclosureGroup("Strumenti per lo sviluppo (pacchetti, framework, librerie...)") {
     Text("Alcune funzionalità di Wardrobe Wizard sono state rese possibili impiegando i seguenti strumenti, i cui autori si ringraziano:")
      .font(.body)
     ForEach(libraryAttributions, id: \.self) { attr in
      VStack {
       Text(attr.title)
        .font(.caption)
       Text(attr.copyright)
        .font(.caption)
       Link(attr.url, destination: URL(string: attr.url)!)
       if attr.license == LibraryAttribution.mitLicence {
        Text("MIT License")
       }
       Text(attr.license)
       if attr != libraryAttributions.last! {
        Divider()
       }
      }
      .font(.footnote)
     }
    }
    DisclosureGroup("Suoni") {
     Text("Wardrobe Wizard utilizza i seguenti suoni tratti da www.freesound.org:")
      .font(.body)
     ForEach(soundAttributions, id: \.self) { attr in
      VStack {
       Text("\"\(attr.title)\" di \(attr.author)")
        .font(.caption)
       Text("Licenza: \(attr.license)")
       Link(attr.url, destination: URL(string: attr.url)!)
       if attr != soundAttributions.last! {
        Divider()
       }
      }
      .font(.footnote)
     }
    }
    DisclosureGroup("Altro") {
     Text("Face ID, Touch ID e iCloud sono marchi registrati di proprietà di Apple Inc.")
      .font(.footnote)
    }
   }
  }
  .navigationTitle("Informazioni su Wardrobe Wizard")
  .navigationBarTitleDisplayMode(.inline)
 }
 
 let libraryAttributions = [
  LibraryAttribution(title: "CloudKitSyncMonitor", copyright: "Copyright (c) 2020 Grant Grueninger", url: "https://github.com/ggruen/CloudKitSyncMonitor"),
  LibraryAttribution(title: "SwiftNFC", copyright: "Copyright (c) 2023 MING", url: "https://github.com/1998code/SwiftNFC"),
  LibraryAttribution(title: "SwiftUIMailView", copyright: "Copyright (c) 2021 Gordan Glavaš", url: "https://github.com/globulus/swiftui-mail-view"),
  LibraryAttribution(title: "Inferno", copyright: "Copyright (c) 2023 Paul Hudson and other authors", url: "https://github.com/twostraws/Inferno")
 ].sorted(by: { $0.title < $1.title })
 let soundAttributions = [
  SoundAttribution(title: "Glimmer.wav", author: "opticaillusions", license: "Creative Commons 0", url: "https://freesound.org/s/521873/"),
  SoundAttribution(title: "Magic mallet.wav", author: "Hotlavaman", license: "Creative Commons 0", url: "https://freesound.org/s/349201/"),
  SoundAttribution(title: "GLEAM-GLOW-SFX-CHIME.wav", author: "newagesoup", license: "Creative Commons 0", url: "https://freesound.org/s/351408/"),
  SoundAttribution(title: "revealed.wav", author: "rmukx", license: "Creative Commons 0", url: "https://freesound.org/s/119457/"),
  SoundAttribution(title: "PuffOfSmoke.wav", author: "euanmj", license: "Creative Commons 0", url: "https://freesound.org/s/442873/")
 ].sorted(by: { $0.title < $1.title })
 struct LibraryAttribution: Hashable {
  static let mitLicence = """
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
  let title: String
  let copyright: String
  let license: String = Self.mitLicence
  let url: String
 }
 struct SoundAttribution: Hashable {
  let title: String
  let author: String
  let license: String
  let url: String
 }
}

