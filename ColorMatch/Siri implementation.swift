//
//  Siri implementation.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 25/06/23.
//

/*
import Foundation
import AppIntents
import SwiftData

struct AddClothingIntent: AppIntent {
 static var title: LocalizedStringResource = "Aggiungi un capo"
 static var description = IntentDescription("Aggiungi un nuovo capo al guardaroba corrente.")
 @Parameter(title: "Tipo", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Di che tipo è il nuovo capo?"), optionsProvider: TypeOptionsProvider())
 var type: String
 private struct TypeOptionsProvider: DynamicOptionsProvider {
  func results() async throws -> [String] {
   var settings: Settings?
   let container = try! ModelContainer(for: Wardrobes.self, Settings.self)
   let context = ModelContext(container)
   // load settings
   let settingsRequest = FetchDescriptor<Settings>()
   let settingsData = try? context.fetch(settingsRequest)
   settings = settingsData?.first ?? Settings()
   return settings?.types ?? [String]()
  }
 }
 @Parameter(title: "Nome del nuovo capo", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Qual è il nome?"))
 var name: String
 @Parameter(title: "Colore", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Di che colore è?"))
 var color: String?
 @Parameter(title: "Fantasia", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Qual'è la fantasia?"))
 var pattern: String?
 @Parameter(title: "Note", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Vuoi aggiungere delle note?"))
 var notes: String?
 @Parameter(title: "Descrizione", inputOptions: String.IntentInputOptions(capitalizationType: .sentences), requestValueDialog: IntentDialog("Vuoi aggiungere una descrizione?"))
 var description: String?
 static var parameterSummary: some ParameterSummary {
  Summary("Crea un nuovo capo di tipo \(\.$type) con il nome \(\.$name).")
 }
 @MainActor func perform() throws -> some IntentResult & ReturnsValue {
  var settings: Settings?
  var wardrobes: Wardrobes?
  // get model context?
  let container = try! ModelContainer(for: Wardrobes.self, Settings.self)
  let context = ModelContext(container)
  // load settings
  let settingsRequest = FetchDescriptor<Settings>()
  let settingsData = try? context.fetch(settingsRequest)
  settings = settingsData?.first ?? Settings()
  // load wardrobes
  let wardrobesRequest = FetchDescriptor<Wardrobes>()
  let wardrobesData = try? context.fetch(wardrobesRequest)
  wardrobes = wardrobesData?.first ?? Wardrobes()
  if let settings {
   if let wardrobes {
    let currentWardrobe = wardrobes.items[settings.lastWardrobe]
    if !settings.types.contains(type) {
     settings.types.append(type)
     context.insert(settings)
    }
    let createdClothing = Clothing(id: UUID(), name: name, type: type, color: color ?? "", patternName: pattern ?? "", notes: notes ?? "", description: description ?? "", matches: [])
    currentWardrobe.clothes.append(createdClothing)
    context.insert(wardrobes)
    print(createdClothing.name, "added to", currentWardrobe.name)
   }
  }
  return .result()
 }
}

struct AddClothingShortcut: AppShortcutsProvider {
 @AppShortcutsBuilder static var appShortcuts: [AppShortcut] {
  AppShortcut(
   intent: AddClothingIntent(),
   phrases: ["Aggiungi un capo con \(.applicationName)", "Aggiungi capo con \(.applicationName)", "Aggiungi un nuovo capo con \(.applicationName)", "Aggiungi nuovo capo con \(.applicationName)", "Aggiungi un capo", "Aggiungi un nuovo capo"],
   shortTitle: "Aggiungi un capo",
   systemImageName: "plus"
  )
 }
 
}
*/

