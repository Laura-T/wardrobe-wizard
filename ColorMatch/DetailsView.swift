//
//  DetailsView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 14/04/23.
//

import SwiftUI
import SwiftData
import TipKit
import UIKit
import CoreNFC

struct DetailsView: View {
 var idTip = IdTip()
 @Query var wardrobes: [Wardrobe]
 var currentClothing: Clothing
 @Environment(Settings.self) var settings
 @State var sort = [SortDescriptor(\Clothing.order)]
 @State var grouping: ClothesGroupingCriteria? = nil
 @State var searchText = ""
 @State var showingEditView = false
 @State var detailWriter = NFCWriter()
 @State private var NFCUnavailableAlert = false
 @State var pasteboard:  UIPasteboard?
 var programString: String {
  switch currentClothing.washingInfo.washingProgram {
  case .unspecified: return String(localized: "non specificato")
  case .darks: return String(localized: "scuri")
  case .darkDelicates: return String(localized: "scuri (delicati)")
  case .lights: return String(localized: "chiari")
  case .lightDelicates: return String(localized: "chiari (delicati)")
  case .darksOther: return String(localized: "scuri (\(currentClothing.washingInfo.programName))")
  case .lightsOther: return String(localized: "chiari (\(currentClothing.washingInfo.programName))")
  }
 }
 var body: some View {
  List {
   if let imageData = currentClothing.image {
    Section("Foto") {
     if let image = UIImage(data: imageData) {
      Image(uiImage: image)
       .resizable()
       .scaledToFit()
     } else {
      Text("Impossibile caricare la foto.")
     }
    }
   }
   Section("Informazioni") {
    if !settings.hideNFCFeatures {
     DisclosureGroup("Id") {
      TipView(idTip, arrowEdge: .bottom)
       .accessibilityForTip()
      HStack {
       Text(currentClothing.id.uuidString)
       Button(action: {
        pasteboard!.string = currentClothing.id.uuidString
        idTip.invalidate(reason: .actionPerformed)
       }, label: {
        VStack {
         if pasteboard?.string == currentClothing.id.uuidString {
          Image(systemName: "doc.on.clipboard")
         } else {
          Image(systemName: "clipboard")
         }
         Text(pasteboard?.string == currentClothing.id.uuidString ? "Copiato" : "Copia")
        }
       })
       Button(action: {
        if !NFCReaderSession.readingAvailable {
         NFCUnavailableAlert = true
        } else {
         detailWriter.startAlert = "Avvicina il telefono al tag che vuoi scrivere."
         detailWriter.msg = currentClothing.id.uuidString
         detailWriter.playSounds = settings.playSounds
         detailWriter.write()
         idTip.invalidate(reason: .actionPerformed)
        }
       }, label: {
        VStack {
         Image(systemName: "phone.and.waveform")
         Text("Scrivi tag")
        }
       })
       .popoverTip(detailWriter.tagPositionTip)
       .alert("NFC non disponibile", isPresented: $NFCUnavailableAlert) {
       } message: {
        Text("Il dispositivo non supporta la tecnologia NFC.")
       }
      }
     }
    }
    Text("Tipo: \(currentClothing.type)")
    Text("Colori: \(currentClothing.unColor.isEmpty ? "Nessuno" : currentClothing.unColor.map { $0.name }.joined(separator: ", "))")
    Text("Fantasia: \(currentClothing.patternName.isEmpty ? "Tinta unita" : currentClothing.patternName)")
    if !currentClothing.notes.isEmpty {
     DisclosureGroup("Note") {
      ForEach(currentClothing.notes.components(separatedBy: "\n"), id: \.self) { paragraph in
       Text(paragraph)
      }
     }
    }
    if !currentClothing.clothingDescription.isEmpty {
     Text("Descrizione: \(currentClothing.clothingDescription)")
    }
    DisclosureGroup(content: {
     Text("Programma: \(programString)")
     if !currentClothing.washingInfo.washingNotes.isEmpty {
      Text(currentClothing.washingInfo.washingNotes)
     }
    }, label: {
     if currentClothing.washingInfo.washingProgram != .unspecified || !currentClothing.washingInfo.washingNotes.isEmpty {
      WateryHeaderView(headerTitle: String(localized: "Informazioni sul lavaggio"))
     } else {
      Text("Informazioni sul lavaggio")
     }
    })
    /*
    if !wardrobes[settings.lastWardrobe].unClothes.filter({ currentClothing.matches.contains($0.id) && !$0.color.isEmpty }).isEmpty {
     DisclosureGroup("Colori abbinati") {
      ForEach(wardrobes[settings.lastWardrobe].unClothes.filter { currentClothing.matches.contains($0.id) && !$0.color.isEmpty }) { clothing in
       Text("Un capo \(clothing.color.lowercased())")
      }
     }
    }
     */
   }
   Section(header: HStack {
    Text("Abbinamenti (\(currentClothing.matches.count - currentClothing.matches.filter { id in !wardrobes[settings.lastWardrobe].unClothes.map({ $0.id }).contains(id) }.count))")
    ClothesSortGroupMenu(sort: $sort, grouping: $grouping)
   }) {
    if !currentClothing.matches.isEmpty {
     ClothesListView(sort: $sort, searchText: $searchText, grouping: $grouping, filterPredicate: { wardrobes[settings.lastWardrobe].unClothes.contains($0) && $0 != currentClothing && currentClothing.matches.contains($0.id) }) { row in
      NavigationLink(row.name, destination: MatchesView(toCompare: [currentClothing, row])).id(UUID())
     }
    } else {
     ContentUnavailableView {
      Text("Nessun abbinamento")
     } description: {
      Text("Non ci sono abbinamenti per questo capo.")
     }
    }
   }
  }
  .onAppear {
   if !settings.hideNFCFeatures {
    pasteboard = UIPasteboard.general
   }
  }
  .navigationTitle("Dettagli \(currentClothing.name)")
  .navigationBarTitleDisplayMode(.inline)
  .toolbar {
   Button(action: {
    showingEditView = true
   }, label: {
    VStack {
     Image(systemName: "pencil")
     Text("Modifica")
    }
   })
   if !settings.hideNFCFeatures {
    NFCFindButton(clothingToFind: currentClothing, detectedDetails: Binding.constant(nil))
   }
  }
  .sheet(isPresented: $showingEditView) {
   ClothingEditorView(currentClothing: currentClothing, showing: $showingEditView)
  }
 }
}

