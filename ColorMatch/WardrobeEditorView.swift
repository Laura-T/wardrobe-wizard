//
//  EditingWardrobeView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 17/07/23.
//

import SwiftUI
import SwiftData

struct WardrobeEditorView: View {
 @Environment(\.modelContext) var modelContext
 @Binding var showing: Bool
 @Query(sort: [SortDescriptor(\Wardrobe.name, comparator: .localizedStandard)]) var wardrobes: [Wardrobe]
 @Environment(Settings.self) var settings
 var selectedWardrobe: Wardrobe?
 var title: String {
  if selectedWardrobe != nil {
   String(localized: "Modifica guardaroba")
  } else {
   String(localized: "Nuovo guardaroba")
  }
 }
 @State private var name = ""
 @State private var selectedClothes = [Clothing]()
 @State private var selectedWardrobes = [Wardrobe]()
 var edited: Bool {
  if selectedWardrobe != nil {
   if name != selectedWardrobe?.name || selectedClothes != selectedWardrobe?.clothes {
    return true
   } else {
    return false
   }
  } else {
   if name != "" || selectedClothes != [] {
    return true
   } else {
    return false
   }
  }
 }
 var body: some View {
  NavigationStack {
   WardrobeForm(name: $name, selectedClothes: $selectedClothes, selectedWardrobes: $selectedWardrobes)
    .interactiveDismissDisabled(edited)
    .navigationTitle(title)
    .navigationBarTitleDisplayMode(.inline)
    .onAppear {
     if let selectedWardrobe {
      name = selectedWardrobe.name
      selectedClothes = selectedWardrobe.unClothes
     }
    }
    .toolbar {
     ToolbarItem(placement: .cancellationAction) {
      CancelButton(edited: edited, showing: $showing)
     }
     ToolbarItem(placement: .confirmationAction) {
      Button(action: {
       if let selectedWardrobe {
        SustainableDeletionTip.hasDeleted = selectedClothes != selectedWardrobe.unClothes && !wardrobes.contains(where: { $0 != selectedWardrobe && $0.unClothes.contains(selectedClothes) })
        print("has deleted:", SustainableDeletionTip.hasDeleted)
        edit(selectedWardrobe, parameters: ["name": name, "selectedClothes": selectedClothes, "selectedWardrobes": selectedWardrobes])
        print("Edited wardrobe: name:", selectedWardrobe.name)
       } else {
        addWardrobe(name: name, importingClothes: selectedClothes, selectedWardrobes: selectedWardrobes)
       }
       if name < wardrobes[settings.lastWardrobe].name {
        settings.lastWardrobe += 1
        modelContext.insert(settings)
       }
       let generator = UINotificationFeedbackGenerator()
       generator.notificationOccurred(.success)
       showing = false
      }, label: {
       VStack {
        Image(systemName: "archivebox")
        Text("Salva")
       }
      })
      .disabled(name.isEmpty)
     }
    }
  }
  .onChange(of: showing) {
   settings.temporaryDisableShake = showing
  }
 }
 func addWardrobe(name: String, importingClothes: [Clothing], selectedWardrobes: [Wardrobe]) {
  print("start:", wardrobes[settings.lastWardrobe].unClothes.map { $0.name })
  let newWardrobe = Wardrobe(name: name, clothes: [], styles: [:])
  modelContext.insert(newWardrobe)
  newWardrobe.clothes = importingClothes
  /*
  for i in importingClothes {
   i.wardrobes?.append(newWardrobe)
   let copy = i.copy() as! Clothing
   for w in wardrobes {
    w.clothes?.removeDuplicates()
    try! modelContext.save()
   }
   print("copied \(i.name),", wardrobes[settings.lastWardrobe].unClothes.map { $0.name })
   print("for i:", i.name, i.id, i.matches)
   var copiedMatches = [UUID]()
   let others = importingClothes.filter { $0 != i }
   print("others:", others.map { ($0.name, $0.id) })
   for o in others {
    if i.matches.contains(o.id) {
     copiedMatches.append(o.id)
     print("o (\(o.name)) is a matches of i (\(i.name)): copied matches =", copiedMatches)
    }
   }
   modelContext.insert(copy)
   print("inserted copy:", copy.name)
   copy.matches = copiedMatches
   newWardrobe.clothes?.append(copy)
   print("appended", copy.name, copy.matches, "clothes =", newWardrobe.unClothes.map { $0.name })
   print(wardrobes[settings.lastWardrobe].unClothes.map { $0.name })
   try! modelContext.save()
   print("saved model context")
  }
   */
  print("clothes:", newWardrobe.clothes?.map { $0.name })
  print(wardrobes[settings.lastWardrobe].unClothes.map { $0.name })
  print("Adding related style info...", selectedWardrobes.map { $0.name })
  for w in selectedWardrobes {
   for key in w.styles.keys {
    print("For key:", key)
    let keyIds = key.components(separatedBy: " + ")
    if keyIds.allSatisfy({ newWardrobe.unClothes.map({ $0.id.uuidString }).contains($0) }) {
     if let style = w.styles[key] {
      print("Updating value:", key, style)
      newWardrobe.styles.updateValue(style, forKey: key)
     }
    } else {
     print("Skipping key:", key)
     continue
    }
   }
  }
  print("wardrobe clothes:", newWardrobe.unClothes.map { $0.name })
  print("wardrobe styles", newWardrobe.styles)
  Wardrobe.logger.notice("Added new wardrobe.")
 }
 func edit(_ wardrobe: Wardrobe, parameters: [String: Any]) {
  wardrobe.name = parameters["name", default: wardrobe.name] as! String
  let selectedClothes = parameters["selectedClothes", default: wardrobe.clothes] as! [Clothing]
  let selectedWardrobes = parameters["selectedWardrobes", default: []] as! [Wardrobe]
  for i in selectedClothes {
   var copiedMatches = [UUID]()
   let others = selectedClothes.filter { $0 != i }
   if !wardrobe.unClothes.contains(i) {
    print("Wardrobe: Running adding block")
    i.wardrobes?.append(wardrobe)
    let copy = i.copy() as! Clothing
    modelContext.insert(copy)
    for o in others {
     if i.matches.contains(o.id) {
      copiedMatches.append(o.id)
     }
    }
    copy.matches = copiedMatches
    wardrobe.clothes?.append(copy)
    try! modelContext.save()
   }
  }
  print("Adding related style info...", selectedWardrobes.map { $0.name })
  for w in selectedWardrobes {
   for key in w.styles.keys {
    print("For key:", key)
    let keyIds = key.components(separatedBy: " + ")
    if keyIds.allSatisfy({ wardrobe.unClothes.map({ $0.id.uuidString }).contains($0) }) {
     if let style = w.styles[key] {
      print("Updating value:", key, style)
      wardrobe.styles.updateValue(style, forKey: key)
     }
    } else {
     print("Skipping key:", key)
     continue
    }
   }
  }
  print("Edited styles:", wardrobe.styles)
  for i in wardrobe.unClothes {
   if !selectedClothes.contains(i) {
    print("Wardrobe: Running removing block")
    if let wardrobeIndex = i.wardrobes?.firstIndex(of: wardrobe) {
     i.wardrobes?.remove(at: wardrobeIndex)
    }
    let others = wardrobe.unClothes.filter { $0 != i }
    for o in others {
     if o.matches.contains(i.id) {
      let ind = o.matches.firstIndex(of: i.id)!
      o.matches.remove(at: ind)
     }
    }
    let ind = wardrobe.unClothes.firstIndex(of: i)!
    wardrobe.clothes?.remove(at: ind)
    for key in wardrobe.styles.keys {
     let matchIds = key.components(separatedBy: " + ")
     if matchIds.contains(i.id.uuidString) {
      print("Removing related style info.")
      wardrobe.styles[key] = nil
     }
    }
   }
  }
  print("inserting in model context...")
  modelContext.insert(wardrobe)
 }
}


