//
//  EditingView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 08/05/23.
//

import SwiftUI
import SwiftData
import TipKit
import NaturalLanguage
import CoreML

struct ClothingEditorView: View {
 @Query var colors: [ClothingColor]
 var immediateTagTip = ImmediateTagTip()
 var justANameTip = JustANameTip()
 @Environment(\.modelContext) var modelContext
 var currentClothing: Clothing?
 var title: String {
  if currentClothing != nil {
   String(localized: "Modifica capo")
  } else {
   String(localized: "Nuovo capo")
  }
 }
 @Binding var showing: Bool
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @State private var name = ""
 @State private var pattern = ""
 @State private var color = ""
 @State private var selectedType = String(localized: "Non impostato")
 @State private var selectedMatches = [Clothing]()
 @State private var notes = ""
 @State private var description = ""
 @State private var washingInfo = WashingInfo()
 @State private var image: UIImage? = nil
 @State private var imageData: Data? = nil
 var edited: Bool {
  if currentClothing != nil {
   if name != currentClothing?.name || color != currentClothing?.unColor.map({ $0.name }).joined(separator: ", ") || pattern != currentClothing?.patternName || notes != currentClothing?.notes || selectedMatches.map({$0.id}) != currentClothing?.matches || description != currentClothing?.clothingDescription || washingInfo != currentClothing?.washingInfo || imageData != currentClothing?.image {
    return true
   } else {
    return false
   }
  } else {
   if name != "" || color != "" || pattern != "" || notes != "" || selectedMatches != [] || description != "" || washingInfo != WashingInfo() || image != nil {
    return true
   } else {
    return false
   }
  }
 }
 @State private var addingWriter = NFCWriter()
 @State var selectedID: UUID?
 @State private var NFCUnavailableAlert = false
 @State var temporaryAutomaticType = false
 @State var addedColors = [ClothingColor]()
 
 var body: some View {
  NavigationStack {
   ClothingForm(immediateTagTip: immediateTagTip, justANameTip: justANameTip, currentId: currentClothing?.id, name: $name, pattern: $pattern, color: $color, selectedType: $selectedType, selectedMatches: $selectedMatches, notes: $notes, description: $description, washingInfo: $washingInfo, image: $image, temporaryAutomaticType: $temporaryAutomaticType, addedColors: $addedColors)
    .interactiveDismissDisabled(edited)
    .onChange(of: image) {
     imageData = image?.pngData()
    }
    .onAppear {
     if let currentClothing {
      selectedType = currentClothing.type
      name = currentClothing.name
      color = currentClothing.unColor.map { $0.name }.joined(separator: ", ")
      pattern = currentClothing.patternName
      notes = currentClothing.notes
      selectedMatches = currentClothing.matches.compactMap { match in
       wardrobes[settings.lastWardrobe].unClothes.first(where: { $0.id == match })
      }
      description = currentClothing.clothingDescription
      washingInfo = currentClothing.washingInfo
      if let data = currentClothing.image {
       image = UIImage(data: data)
      }
     } else {
      selectedID = UUID()
     }
    }
    .navigationTitle(title)
    .navigationBarTitleDisplayMode(.inline)
    .toolbar {
     ToolbarItem(placement: .cancellationAction) {
      CancelButton(edited: edited, showing: $showing)
     }
     ToolbarItem(placement: .confirmationAction) {
      Button(action: {
       var clothingColors = [ClothingColor]()
       var colorOrder = [Int: String]()
       for c in color.components(separatedBy: ", ") {
        if colors.contains(where: { $0.name == c.capitalized }) {
         clothingColors.append(colors.first(where: { $0.name == c.capitalized })!)
        } else if addedColors.contains(where: { $0.name == c.capitalized }) {
         clothingColors.append(addedColors.first(where: { $0.name == c.capitalized })!)
        }
        colorOrder[color.components(separatedBy: ", ").firstIndex(of: c)!] = c.capitalized
       }
       print("clothingColors =", clothingColors.map { $0.name })
       if let currentClothing {
        wardrobes[settings.lastWardrobe].edit(currentClothing, parameters: ["type": selectedType, "name": name, "color": clothingColors, "colorOrder": colorOrder, "pattern": pattern, "notes": notes, "description": description, "washingInfo": washingInfo, "matches": selectedMatches.map({ $0.id }), "image": imageData])
       } else {
        var definedType = selectedType
        if temporaryAutomaticType {
         for t in settings.types {
          if name.lowercased().hasPrefix(t.lowercased()) {
           definedType = t
          }
         }
         if !settings.types.contains(String(name.prefix(while: { $0 != " " }))) {
          let newType = String(name.prefix(while: { $0 != " " }))
          definedType = newType
          settings.types.append(newType)
          if settings.sortTypes {
           settings.types.sort()
           settings.types.move(fromOffsets: [settings.types.firstIndex(of: String(localized: "Non impostato"))!], toOffset: 0)
          }
         }
        }
        let createdClothing = Clothing(id: selectedID!, name: name, type: definedType, color: clothingColors, dominantColor: clothingColors.first, colorOrder: colorOrder, patternName: pattern, notes: notes, description: description, washingInfo: washingInfo, matches: selectedMatches.map({ $0.id }), image: imageData)
        wardrobes[settings.lastWardrobe].add(createdClothing, addingMatches: selectedMatches.map({ $0.id }))
       }
       if let currentClothing {
        print("current clothing color:", currentClothing.color?.map { $0.name })
        for w in wardrobes {
         print(w.name, w.unClothes.first(where: { $0 == currentClothing })?.name, w.unClothes.first(where: { $0 == currentClothing })?.matches)
        }
       }
       modelContext.insert(wardrobes[settings.lastWardrobe])
       print("inserted clothing")
       if let currentClothing {
        print("current clothing color:", currentClothing.color?.map { $0.name })
        for w in wardrobes {
         print(w.name, w.unClothes.first(where: { $0 == currentClothing })?.name, w.unClothes.first(where: { $0 == currentClothing })?.matches)
        }
       }
       justANameTip.invalidate(reason: .actionPerformed)
       let generator = UINotificationFeedbackGenerator()
       generator.notificationOccurred(.success)
       showing = false
      }, label: {
       VStack {
        Image(systemName: "archivebox")
        Text("Salva")
       }
      })
      .disabled(name.isEmpty)
      .simultaneousGesture(
       LongPressGesture()
        .onEnded { _ in
         if currentClothing == nil && !name.isEmpty && !settings.hideNFCFeatures {
          if ProcessInfo.processInfo.isiOSAppOnMac {
           NFCUnavailableAlert = true
          } else {
           addingWriter.startAlert = "Avvicina il telefono al tag che vuoi scrivere."
           addingWriter.msg = selectedID!.uuidString
           addingWriter.write()
           immediateTagTip.invalidate(reason: .actionPerformed)
          }
         }
        }
      )
      .accessibilityHint(currentClothing == nil && !settings.hideNFCFeatures ? "Tocca due volte e tieni premuto per associare il nuovo capo a un tag NFC." : "")
      .popoverTip(addingWriter.tagPositionTip)
      .alert("NFC non disponibile", isPresented: $NFCUnavailableAlert) {
      } message: {
       Text("MacOS non supporta la tecnologia NFC. Utilizza un iPhone per leggere e scrivere i tag.")
      }
     }
    }
    .onChange(of: showing) {
     settings.temporaryDisableShake = showing
    }
  }
 }
}



