//
//  SettingsView.swift
//  ColorMatch
//
//  Created by Laura Tosetto on 14/06/23.
//

import SwiftUI
import SwiftData
import TipKit

struct SettingsView: View {
 var hideNFCTip: HideNFCTip? = nil
 @Environment(\.modelContext) var modelContext
 @Environment(Settings.self) var settings
 @Query var wardrobes: [Wardrobe]
 @Query var colors: [ClothingColor]
 @State private var currentType = ""
 @State private var showingTypeAlert = false
 @State private var index = 0
 @State private var newType = false
 @State private var typeExistsAlert = false
 @State private var deleteTypeAlert = false
 @State private var toDelete: IndexSet = []
 @State private var typesToDelete = ""
 @State private var resetTypesAlert = false
 @State private var resetWardrobesAlert = false
 @State private var resetColorsAlert = false
 
 func delete(offsets: IndexSet) {
  var removed = [String]()
  let indices = Array(offsets)
  for n in indices {
   let current = settings.types[n]
   removed.append(current)
  }
   for w in wardrobes {
    for i in w.unClothes {
     for removedItem in removed {
      if i.type == removedItem {
       i.type = String(localized: "Non impostato")
      }
     }
    }
    modelContext.insert(w)
   }
  settings.types.remove(atOffsets: offsets)
 }
 func move(from source: IndexSet, to destination: Int) {
  settings.types.move(fromOffsets: source, toOffset: destination)
  modelContext.insert(settings)
 }
 var body: some View {
  @Bindable var settings = settings
  Form {
   Section("Schermata principale") {
    Toggle("Mostra il nome del guardaroba attuale", isOn: $settings.showWardrobeName)
    Text("Mostra il nome del guardaroba attuale nella schermata principale.")
    Picker("Ordina capi per:", selection: $settings.currentClothesSorting) {
     Text("Nome (a - z)").tag(Settings.ClothesSortSetting.name)
     Text("Colore dominante (a - z)").tag(Settings.ClothesSortSetting.colorName)
     Text("Colore dominante (caldo - freddo)").tag(Settings.ClothesSortSetting.colorTemperatureWarm)
     Text("Colore dominante (freddo - caldo)").tag(Settings.ClothesSortSetting.colorTemperatureCold)
     Text("Ordinamento personalizzato").tag(Settings.ClothesSortSetting.order)
    }.pickerStyle(.inline)
    Text("Scegli come ordinare i capi mostrati in tutte le schermate dell'app. Puoi sempre modificare temporaneamente quest'impostazione in ogni schermata rilevante.")
    Picker("Raggruppa capi per:", selection: $settings.clothesGrouping) {
     Text("Nessuno").tag(Optional<ClothesGroupingCriteria>.none)
     Text("Nome").tag(ClothesGroupingCriteria.name as ClothesGroupingCriteria?)
     Text("Tipo").tag(ClothesGroupingCriteria.type as ClothesGroupingCriteria?)
     Text("Colore dominante").tag(ClothesGroupingCriteria.color as ClothesGroupingCriteria?)
     Text("Temperatura colore dominante").tag(ClothesGroupingCriteria.temperature as ClothesGroupingCriteria?)
    }.pickerStyle(.inline)
    Text("Scegli se e come raggruppare i capi mostrati in tutte le schermate dell'app. Puoi sempre modificare temporaneamente quest'impostazione in ogni schermata rilevante.")
   }
   Section("Schermata \"Elenco Guardaroba\"") {
    Toggle("Mostra il conteggio dei capi", isOn: $settings.showClothesCount)
    Text("Mostra il numero di capi presenti in ogni guardaroba.")
    Toggle("Mostra l'indicatore del guardaroba attuale", isOn: $settings.showWardrobeIndicator)
    Text("Indica il guardaroba attualmente selezionato nell'elenco guardaroba e quando aggiungi o modifichi un guardaroba.")
   }
   Section("Funzionalità NFC") {
    Toggle("Nascondi funzionalità NFC", isOn: $settings.hideNFCFeatures)
     .onChange(of: settings.hideNFCFeatures) {
      if settings.hideNFCFeatures {
       hideNFCTip?.invalidate(reason: .actionPerformed)
      }
     }
    Text("Se non utilizzi i tag NFC per distinguere i tuoi capi, puoi nascondere i relativi controlli da tutte le schermate dell'app.")
    Toggle("Annuncia il nome del guardaroba", isOn: $settings.speakWardrobeName)
     .disabled(wardrobes.count == 1)
    Text("Quando riconosci un capo, mostra i guardaroba in cui è incluso. Questa opzione non è disponibile se è presente solo un guardaroba.")
    Toggle("Riproduci suoni", isOn: $settings.playSounds)
    Text("Riproduci un effetto sonoro quando leggi o scrivi un tag.")
   }
   Section("Altre impostazioni") {
    Toggle("Sblocca l'app con dati biometrici", isOn: $settings.useBiometrics)
    Text("Se attivo, devi autenticarti con Face ID o Touch ID per poter accedere ai tuoi capi.")
    Toggle("Evidenzia selezione", isOn: $settings.highlightSelection)
    Text("Se attivo, gli elementi selezionati negli elenchi verranno circondati da un rettangolo animato.")
    Toggle("Agita per \"Abbinamento casuale\"", isOn: $settings.shakeForRandomMatch)
    Text("Se attivo, potrai ricevere un suggerimento a caso tra i tuoi abbinamenti agitando il dispositivo. Questa funzionalità è disponibile solo nella schermata principale.")
   }
   Section("Tipi di capo") {
    Toggle("Inserisci automaticamente tipi", isOn: $settings.automaticTypes)
    Text("Se l'impostazione è attiva, il tipo di capo verrà ricavato automaticamente dai nomi dei nuovi capi che aggiungi. Ad esempio, un capo che si chiama \"Pantaloni verdi\" verrà automaticamente classificato con il tipo \"Pantaloni\". Se quest'ultimo non è presente nella lista dei tipi disponibili (che puoi modificare qui sotto), verrà aggiunto automaticamente.")
    Toggle("Ordina alfabeticamente", isOn: $settings.sortTypes)
     .onChange(of: settings.sortTypes) {
      if settings.sortTypes {
       settings.types.sort()
       settings.types.move(fromOffsets: [settings.types.firstIndex(of: String(localized: "Non impostato"))!], toOffset: 0)
      }
     }
     .onChange(of: settings.types) {
      if settings.sortTypes {
       settings.types.sort()
       settings.types.move(fromOffsets: [settings.types.firstIndex(of: String(localized: "Non impostato"))!], toOffset: 0)
      }
     }
    Text("Se l'impostazione è attiva, non potrai riordinare i tipi nell'elenco qui sotto.")
    Section(header: HStack {
     Text("Personalizza tipi di capo")
     Button(action: {
      newType = true
      showingTypeAlert = true
      currentType = ""
     }, label: {
      VStack {
       Image(systemName: "plus")
       Text("Aggiungi")
      }
     })
     if !settings.sortTypes {
      EditButton()
     }
    }) {
     ForEach(settings.types, id: \.self) { type in
      if type != String(localized: "Non impostato") {
       Button(type) {
        showingTypeAlert = true
        currentType = type
        index = settings.types.firstIndex(of: type)!
       }
      }
     } // for each
     .onMove(perform: move)
     .onDelete(perform: { indices in
      deleteTypeAlert = true
      toDelete = indices
      var names = [String]()
      for i in indices {
       names.append(settings.types[i])
      }
      typesToDelete = names.joined(separator: ", ")
     })
     .alert("Elimina tipo", isPresented: $deleteTypeAlert) {
      Button("Annulla", role: .cancel) {
       toDelete = []
       typesToDelete = ""
      }
      Button("Elimina", role: .destructive) {
       delete(offsets: toDelete)
       toDelete = []
       typesToDelete = ""
      }
     } message: {
      Text("Confermi di voler eliminare il tipo \(typesToDelete)? I relativi capi non verranno eliminati, ma il loro tipo risulterà non impostato.")
     }
    }
   }
   Section("Ripristina default") {
    Button("Ripristina tipi") {
     resetTypesAlert = true
    }
    .alert("Ripristina tipi", isPresented: $resetTypesAlert) {
     Button("Annulla", role: .cancel) { }
     Button("Ripristina", role: .destructive) {
      settings.types = [String(localized: "Non impostato"), String(localized: "Camicia"), String(localized: "Felpa"), String(localized: "Gonna"), String(localized: "Maglietta"), String(localized: "Maglione"), String(localized: "Pantaloni"), String(localized: "Vestito intero")]
      for w in wardrobes {
       for i in w.unClothes {
        if !settings.types.contains(i.type) {
         i.type = String(localized: "Non impostato")
        }
       }
       modelContext.insert(w)
      }
      modelContext.insert(settings)
     }
    } message: {
     Text("Confermi di voler ripristinare i tipi di capo ai valori di default? Tutti i tipi personalizzati inseriti verranno eliminati. L'azione è irreversibile.")
    }
    Button("Ripristina colori") {
     resetColorsAlert = true
    }
    .alert("Ripristina colori", isPresented: $resetColorsAlert) {
     Button("Annulla", role: .cancel) { }
     Button("Ripristina", role: .destructive) {
      try! modelContext.delete(model: ClothingColor.self)
      for w in wardrobes {
       for c in w.unClothes {
        c.color = []
       }
       modelContext.insert(w)
      }
     }
    } message: {
     Text("Confermi di voler ripristinare i dati relativi ai colori? Tutti i colori inseriti verranno eliminati e rimossi da tutti i capi. L'azione è irreversibile.")
    }
    Button("Ripristina guardaroba") {
     resetWardrobesAlert = true
    }
    .alert("Ripristina guardaroba", isPresented: $resetWardrobesAlert) {
     Button("Annulla", role: .cancel) { }
     Button("Ripristina") {
      /*
      for c in colors {
       for clothing in c.unColoredClothes {
        modelContext.delete(clothing)
       }
       for clothing in c.dominants! {
        modelContext.delete(clothing)
       }
       modelContext.insert(c)
      }
       */
      try! modelContext.delete(model: Clothing.self)
      try! modelContext.delete(model: Wardrobe.self)
      modelContext.insert(Wardrobe(name: "Default"))
      settings.lastWardrobe = 0
      modelContext.insert(settings)
     }
    } message: {
     Text("Confermi di voler ripristinare i dati dei guardaroba? Tutti i guardaroba creati e i capi che contengono verranno eliminati. L'azione è irreversibile.")
    }
   }
  } // form
  .alert(newType ? "Nuovo tipo" : "Modifica tipo", isPresented: $showingTypeAlert) {
   TextField("Inserisci nome", text: $currentType)
   Button("Annulla", role: .cancel) { }
   Button("OK") {
    if !newType {
     for w in wardrobes {
      for c in w.unClothes {
       if c.type == settings.types[index] {
        c.type = currentType
       }
      }
      modelContext.insert(w)
     }
     settings.types[index] = currentType
    } else {
     if settings.types.contains(currentType) {
      typeExistsAlert = true
     } else {
      settings.types.append(currentType)
     }
    }
   }
  }
  .alert("Tipo già esistente", isPresented: $typeExistsAlert) {
  } message: {
   Text("Il tipo \"\(currentType)\" esiste già. Prova con un nome diverso.")
  }
  .navigationTitle("Impostazioni")
  .navigationBarTitleDisplayMode(.inline)
  .onDisappear {
   modelContext.insert(settings)
  }
 } // body
} // struct

